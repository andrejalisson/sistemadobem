<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendedorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendedor', function (Blueprint $table) {
            $table->increments('id_ven');
            $table->string('nome_ven', 100);
            $table->string('responsavel_ven', 100)->nullable();
            $table->string('telefone_ven', 11)->nullable();
            $table->integer('tipo_ven');
            
            $table->string('enderecoC_ven', 85)->nullable();
            $table->string('bairroC_ven', 45)->nullable();
            $table->string('cidadeC_ven', 45)->nullable();
            $table->string('referenciaC_ven', 45)->nullable();

            $table->string('enderecoR_ven', 85)->nullable();
            $table->string('bairroR_ven', 45)->nullable();
            $table->string('cidadeR_ven', 45)->nullable();
            $table->string('referenciaR_ven', 45)->nullable();

            $table->date('cadastro_ven');
            $table->string('fechamento_ven', 30);
            $table->integer('distribuidorId_ven');
            $table->boolean('status_ven')->default(true);            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendedor');
    }
}
