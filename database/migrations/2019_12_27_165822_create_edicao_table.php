<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEdicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('edicao', function (Blueprint $table) {
            $table->increments('id_edc');
            $table->integer('numero_edc');
            $table->date('sorteio_edc');
            $table->date('liberacao_edc');
            $table->integer('globo_edc');
            $table->integer('giro_edc');
            $table->boolean('dupla_edc');
            $table->integer('saltoDupla_edc');
            $table->integer('qtdCertificados_edc');
            $table->integer('tipoCertificados_edc');
            $table->boolean('status_edc');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edicao');
    }
}
