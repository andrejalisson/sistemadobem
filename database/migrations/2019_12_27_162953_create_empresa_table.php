<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpresaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresa', function (Blueprint $table) {
            $table->increments('id_emp');
            $table->string('fantasia_emp', 100);
            $table->string('cnpj_emp', 14);
            $table->string('razaosocial_emp', 120);
            $table->string('ie_emp', 15);
            $table->string('im_emp', 15);
            $table->string('cep_emp', 8);
            $table->string('lagradouro_emp', 80);
            $table->integer('numero_emp');
            $table->string('bairro_emp', 100);
            $table->string('cidade_emp', 100);
            $table->string('estado_emp', 100);
            $table->boolean('status_emp')->default(false);





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresa');
    }
}
