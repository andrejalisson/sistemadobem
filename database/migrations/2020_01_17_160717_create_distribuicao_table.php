<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistribuicaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribuicao', function (Blueprint $table) {
            $table->increments('id_dist');
            $table->integer('inicial_dist');
            $table->integer('final_dist');
            $table->integer('distribuidor_dist');
            $table->integer('edicao_dist');
            $table->date('liberacao_dist');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribuicao');
    }
}
