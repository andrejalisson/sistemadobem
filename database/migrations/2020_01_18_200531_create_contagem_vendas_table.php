<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContagemVendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contagem_vendas', function (Blueprint $table) {
            $table->increments('id_cven');
            $table->string('certificado_cven', 6);
            $table->string('edicao_cven', 5);
            $table->integer('distribuidor_cven');
            $table->boolean('processado_cven')->default(False);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contagem_vendas');
    }
}
