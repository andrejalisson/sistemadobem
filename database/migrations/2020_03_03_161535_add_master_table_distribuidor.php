<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMasterTableDistribuidor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('distribuidor', function (Blueprint $table) {
            $table->integer('masterId_dis')
                            ->nullable()
                            ->after('regiaoId_dis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::table('distribuidor', function (Blueprint $table) {
            $table->dropColumn('masterId_dis');
        });
    }
}
