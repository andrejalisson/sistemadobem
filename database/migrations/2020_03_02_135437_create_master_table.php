<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master', function (Blueprint $table) {
            $table->increments('id_mas');
            $table->string('nome_mas', 100);
            $table->string('responsavel_mas', 100)->nullable();
            $table->string('telefone_mas', 16)->nullable();
            $table->integer('regiaoId_mas');
            $table->boolean('status_mas')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master');
    }
}
