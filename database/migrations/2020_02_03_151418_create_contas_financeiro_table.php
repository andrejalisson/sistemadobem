<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContasFinanceiroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contas_financeiro', function (Blueprint $table) {
            $table->increments('id_cfin');
            $table->string('nome_cfin');
            $table->decimal('saldo_cfin', 8, 2);
            $table->date('criacao_cfin')->nullable();
            $table->date('alteracao_cfin')->nullable();
            $table->boolean('status_cfin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contas_financeiro');
    }
}
