<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistribuidorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('distribuidor', function (Blueprint $table) {
            $table->increments('id_dis');
            $table->string('nome_dis', 100);
            $table->string('responsavel_dis', 100)->nullable();
            $table->string('telefone_dis', 11)->nullable();
            $table->integer('regiaoId_dis');
            $table->boolean('status_dis')->default(true);
          });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('distribuidor');
    }
}
