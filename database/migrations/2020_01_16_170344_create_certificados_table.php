<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificados', function (Blueprint $table) {
            $table->increments('id_cer');
            $table->integer('numero_cer');
            $table->decimal('valor_cer', 8, 2);
            $table->integer('edicao_cer');
            $table->integer('distribuidor_id');
            $table->integer('vendedor_id');
            $table->boolean('vendido_cer')->nullable()->default(false);
            $table->boolean('devolvido_cer')->nullable()->default(false);
            $table->boolean('status_cer')->nullable()->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificados');
    }
}
