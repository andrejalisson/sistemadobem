<?php

namespace App\Http\Middleware;

use Closure;

class VerificaAutenticacao
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
        if (!$request->session()->exists('logado')) {
            session()->flush();
            $request->session()->flash('alerta', 'Você não tem acesso a essa página.');
            return redirect('/Login');
        }else{
            return $next($request);
        }        
    }
}
