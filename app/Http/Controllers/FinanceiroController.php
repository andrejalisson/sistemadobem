<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FinanceiroController extends Controller{

    public function contas(){
        $title = "Contas";
        return view('financeiro.listarContas')->with(compact('title'));
    }

    public function todasContas(Request $request){
        $columns = array(
            0 =>'id_cfin',
            1 =>'nome_cfin',
            2 =>'saldo_cfin',
        );
        $totalData = DB::table('contas_financeiro')
                        ->where('status_cfin', 1)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $contas = DB::table('contas_financeiro')
                            ->where('status_cfin', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $contas =  DB::table('contas_financeiro')
                            ->where('nome_cfin','LIKE',"%{$search}%")
                            ->where('status_cfin', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('contas_financeiro')
                            ->where('nome_cfin','LIKE',"%{$search}%")
                            ->where('status_cfin', 1)
                            ->count();
        }
        $data = array();
        if(!empty($contas)){
            foreach ($contas as $contas){
                $nestedData['id'] = "# ".$contas->id_cfin;
                $nestedData['nome'] = $contas->nome_cfin;
                $nestedData['saldo'] = "R$ ".$contas->saldo_cfin;
                $view = "";
                $editar = "onclick=\"location.href='/Regioes/Editaredicao/".$contas->id_cfin."'\"";
                $excluir = "onclick=\"location.href='/Regioes/Excluiredicao/".$contas->id_cfin."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-primary btn-circle\"  type=\"button\"><i class=\"far fa-eye\"></i></button>
                                            <button class=\"btn btn-warning btn-circle\" ".$contas->id_cfin. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\"  type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
