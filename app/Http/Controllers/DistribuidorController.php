<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DistribuidorController extends Controller{

    public function lista(){
        $title = "Distribuidor";
        return view('distribuidor.lista')->with(compact('title'));
    }

    public function add(){
        $regiao = DB::table('regiao')->where('status_reg', 1)->get();
        $master = DB::table('master')->where('status_mas', 1)->get();
        $title = "Adicionar Distribuidor";
        return view('distribuidor.add')->with(compact('title', 'regiao', 'master'));
    }

    public function addPost(Request $request){
        $telefone = str_replace('(', '', $request->telefone);
        $telefone = str_replace(')', '', $telefone);
        $telefone = str_replace('-', '', $telefone);
        $telefone = str_replace(' ', '', $telefone);
        DB::table('distribuidor')->insert([
            'nome_dis' => $request->nome,
            'responsavel_dis' => $request->responsavel,
            'telefone_dis' => $telefone,
            'regiaoId_dis' => $request->regiao,
            'masterId_dis' => $request->master,
            'status_dis' => true,
            ]
        );
        $request->session()->flash('sucesso', 'Distribuidor adicionado com sucesso!');
        return redirect('/Distribuidores/Listar');
    }

    public function todosDistribuidores(Request $request){
        $columns = array(
            0 =>'id_dis',
            1 =>'nome_dis',
            2 =>'responsavel_dis',
            3 =>'telefone_dis',
            4 =>'nome_mas',
            5 =>'nome_reg',
        );
        
        $totalData = DB::table('distribuidor')
                        ->where('status_dis', 1)
                        ->count();
        
        
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $distribuidores = DB::table('distribuidor')
                            ->leftJoin('regiao', 'regiaoId_dis', '=', 'id_reg')
                            ->leftJoin('master', 'masterId_dis', '=', 'id_mas')
                            ->where('status_dis', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            
            
        }
        else{
            $search = $request->input('search.value');
            $distribuidores =  DB::table('distribuidor')
                            ->leftJoin('regiao', 'regiaoId_dis', '=', 'id_reg')
                            ->leftJoin('master', 'masterId_dis', '=', 'id_mas')
                            ->where('nome_dis','LIKE',"%{$search}%")
                            ->orWhere('responsavel_dis','LIKE',"%{$search}%")
                            ->orWhere('telefone_dis','LIKE',"%{$search}%")
                            ->orWhere('nome_reg','LIKE',"%{$search}%")
                            ->where('status_reg', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('distribuidor')
                            ->leftJoin('regiao', 'regiaoId_dis', '=', 'id_reg')
                            ->leftJoin('master', 'masterId_dis', '=', 'id_mas')
                            ->where('nome_dis','LIKE',"%{$search}%")
                            ->orWhere('responsavel_dis','LIKE',"%{$search}%")
                            ->orWhere('telefone_dis','LIKE',"%{$search}%")
                            ->orWhere('nome_reg','LIKE',"%{$search}%")
                            ->where('status_reg', 1)
                            ->count();
            
        }
        $data = array();
        if(!empty($distribuidores)){
            foreach ($distribuidores as $distribuidor){
                $nestedData['id'] = "# ".$distribuidor->id_dis;
                $nestedData['nome'] = $distribuidor->nome_dis;
                $nestedData['responsavel'] = $distribuidor->responsavel_dis;
                $nestedData['master'] = $distribuidor->nome_mas;
                $nestedData['regiao'] = $distribuidor->nome_reg;
                $nestedData['telefone'] = $distribuidor->telefone_dis;
                $view = "";
                $editar = "onclick=\"location.href='/Distribuidores/Editar/".$distribuidor->id_dis."'\"";
                $excluir = "onclick=\"location.href='/Distribuidores/Excluir/".$distribuidor->id_dis."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\" ".$excluir. " type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }

    public function editar($id){
        $title = "Editar Distribuidor";
        $distribuidor = DB::table('distribuidor')->where('id_dis', $id)->first();
        $regiao = DB::table('regiao')->where('status_reg', 1)->get();
        $master = DB::table('master')->where('status_mas', 1)->get();
        return view('distribuidor.editar')->with(compact('title', 'distribuidor','regiao', 'master'));
    }

    public function excluir(Request $request, $id){
        DB::table('distribuidor')
                ->where('id_dis', $id)
                ->update(['status_dis' => false]);
        $request->session()->flash('sucesso', 'Distribuidor Excluido com Sucesso!');
        return redirect('/Distribuidores/Listar');
    }

    public function editarPost(Request $request, $id){
        $telefone = str_replace('(', '', $request->telefone);
        $telefone = str_replace(')', '', $telefone);
        $telefone = str_replace('-', '', $telefone);
        $telefone = str_replace(' ', '', $telefone);

        DB::table('distribuidor')
                ->where('id_dis', $id)
                ->update([
                    'nome_dis' => $request->nome,
                    'responsavel_dis' => $request->responsavel,
                    'telefone_dis' => $telefone,
                    'regiaoId_dis' => $request->regiao,
                    'masterId_dis' => $request->master,
                    ]);
        $request->session()->flash('sucesso', 'Distribuidor Editado com sucesso!');
        return redirect('/Distribuidores/Listar');
    }

    public function migrar(){
        $distribuidores = DB::connection('mysql')->table('a_revendedor')->get();
        foreach ($distribuidores as $distribuidores) {   
            DB::table('distribuidor')->insert([
                'nome_dis' => $distribuidores->NM_REVENDEDOR,
                'regiaoId_dis' => $distribuidores->CD_REGIONAL,
                ]
            );
        } 
        return redirect('/Distribuidores/Listar');
    }  

}
