<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProcessamentoController extends Controller{

    public function devolucaoIGS(){
        $title = "Devolução";
        $distribuidor = DB::table('distribuidor')->where('status_dis', 1)->get();
        return view('processamento.devolucaoIGS')->with(compact('title','distribuidor'));
    }

    public function contagemVenda(){
        $title = "Contagem de Venda Certificados Primários";
        return view('processamento.contagemVenda')->with(compact('title'));
    }

    public function contagemVendaSecundario(){
        $title = "Contagem de Venda Certificados Secundários";
        return view('processamento.contagemVendaSecundario')->with(compact('title'));
    }

    public function devolucaoBIP(){
        $title = "Devolução BIP";
        return view('processamento.devolucaoBIP')->with(compact('title'));
    }

    public function carregarCodigo(){
        $certificado = DB::table('contagem_vendas')->where('edicao_cven', "008")->get();
        foreach ($certificado as $certificado) {
            DB::table('contagem_vendas')
                ->where('certificado_cven', $certificado->certificado_cven)
                ->update(['codigo_cven' => intval($certificado->certificado_cven)]
            );
            echo $certificado->certificado_cven."<br>";
        }
    }

    public function devolucao(Request $request){
        $total = DB::table('certificados')->wherein('numero_cer', $request->certificadosIGS)->where('vendido_cer', false)->where('devolvido_cer', false)->where('edicao_cer', intval($request->edcIGS))->count();
        if ($total != 0) {
            DB::table('certificados')
                ->wherein('numero_cer', $request->certificadosIGS)
                ->where('edicao_cer',  intval($request->edcIGS))
                ->update(['devolvido_cer' => true]
            );
        }
        return $total;
    }

    public function contagemVendaPost(Request $request){
        $total = DB::table('certificados')->wherein('numero_cer', $request->certificadosIGS)->where('vendido_cer', false)->where('edicao_cer', intval($request->edcIGS))->count();
        if ($total != 0) {
            DB::table('certificados')
                ->wherein('numero_cer', $request->certificadosIGS)
                ->where('edicao_cer',  intval($request->edcIGS))
                ->update(['vendido_cer' => true,'devolvido_cer' => false]);
            
        }
        return $total;
    }


    public function contagemVendaSecundarioPost(Request $request){
        $total = DB::table('certificados')->wherein('numero_cer', $request->certificadosIGS)->where('vendido_cer', false)->where('edicao_cer', intval($request->edcIGS))->count();
        if ($total != 0) {
            DB::table('certificados')
                ->wherein('numero_cer', $request->certificadosIGS)
                ->where('edicao_cer',  intval($request->edcIGS))
                ->update(['vendido_cer' => true,'devolvido_cer' => false, 'valor_cer' => 2.00]
            );
        }
        return $total;
    }

    public function atualizar(){
        // $vendas = DB::table('contagem_vendas')->where('processado_cven', false)->where('edicao_cven', "008")->get();
        // $Vendidos = 0;
        $Devolvidos = 0;
        // foreach ($vendas as $vendas){
        //     DB::table('certificados')
        //         ->where('numero_cer', $vendas->certificado_cven)
        //         ->where('edicao_cer',  8)
        //         ->update(['vendido_cer' => true]
        //     );
        //     $Vendidos++;
        //     echo $Vendidos." - ".$vendas->certificado_cven." - Vendido <BR>";
        // }
        $devolvidos = DB::table('devolucao')->where('processado_dev', false)->where('edicao_dev', "008")->get();
        foreach ($devolvidos as $devolvidos){
            DB::table('certificados')
                ->where('numero_cer', $devolvidos->certificado_dev)
                ->where('edicao_cer',  8)
                ->update(['devolvido_cer' => true]
            );
            $Devolvidos++;
            echo $Devolvidos." - ".$devolvidos->certificado_dev." - Devolvido <BR>";
        }
        
    }
}
