<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MatrizController extends Controller{
    public function gerarMatriz(){
        for ($y=0; $y < 50000; $y++) { 
            $n = array();
            $n[] = str_pad(rand(1, 10), 2, '0', STR_PAD_LEFT);
            $n[] = rand(11, 20);
            $n[] = rand(21, 30);
            $n[] = rand(31, 40);
            $n[] = rand(41, 50);
            $n[] = rand(51, 60);
            $i = 1;
            while ($i <= 14) {
                $x = str_pad(rand(1, 60), 2, '0', STR_PAD_LEFT);
                if (!in_array($x,$n)) {
                    $n[] = $x;
                    $i++;
                }
            }
            sort($n);
            $numeros = implode('-', $n);
            $matriz = DB::table('matriz')->where('numeros_mat', $numeros)->first();
            if($matriz == null){
                echo $y." -> ".$numeros."<BR>";
                DB::table('matriz')->insert([
                    'numeros_mat' => $numeros
                    ]
                );
            }
        }
        
    }
}
