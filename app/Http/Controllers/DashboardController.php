<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller{


    
    public function contagemVendas(){
        set_time_limit(3000);
        $title = "Contagem de vendas";
        $distribuidor = DB::table('distribuidor')
        ->orderBy('nome_dis')
        ->get();
        $certificados = DB::table('certificados')->where('edicao_cer', 11)->get();                    
        return view('dashboards.contagemVendas')->with(compact('title', 'distribuidor', 'certificados'));

    }

    public function financeiro(){
        

    }

    public function contagemVendasDistribuidor($id){
        $title = "Contagem de vendas";
        $distribuidor = DB::table('distribuidor')->where('id_dis', $id)->get();
        $certificados = DB::table('certificados')->where('distribuidor_id', $id)->get();        
        return view('dashboards.contagemVendas')->with(compact('title', 'distribuidor', 'certificados'));

    }

    public function processamento(){
        set_time_limit(1800);
        $title = "Processamento";
        $distribuidor = DB::connection('mysql')->table('a_revendedor')
        ->orderBy('NM_REVENDEDOR')
        ->get();
        $certificados = DB::connection('mysql')->table('cartelas_validas')->select('sequencial', 'cd_revendedor')->get();
        
            
            //$distribuidor->ceara5 = $certificados->where('cd_revendedor', $distribuidor->id_dis)->where('sequencial','<=', 60000)->count();
            //$distribuidor->ceara2 = $certificados->where('cd_revendedor', $distribuidor->id_dis)->where('sequencial','>', 60000)->where('sequencial','<=', 120000)->count();
            //$distribuidor->fortaleza5 = $certificados->where('cd_revendedor', $distribuidor->id_dis)->where('sequencial','>', 120000)->where('sequencial','<=', 180000)->count();
            //$distribuidor->fortaleza2 = $certificados->where('cd_revendedor', $distribuidor->id_dis)->where('sequencial','>', 180000)->count();
        
        return view('dashboards.acompanhamento')->with(compact('title', 'distribuidor', 'certificados'));

    }

    public function devolucao(){
        set_time_limit(1800);
        $title = "Devolução";
        $distribuidor = DB::connection('mysql')->table('a_revendedor')
        ->orderBy('NM_REVENDEDOR')
        ->get();
        $certificados = DB::connection('mysql')->table('cartelas_devolvidas')->select('sequencial', 'cd_revendedor')->get();
        return view('dashboards.devolucao')->with(compact('title', 'distribuidor', 'certificados'));
    }

    public function resumo(){
        set_time_limit(1800);
        $title = "Devolução";
        $distribuidor = DB::connection('mysql')->table('a_revendedor')
        ->orderBy('NM_REVENDEDOR')
        ->get();

        $certificadosDistribuidos = DB::connection('mysql')->table('a_distr')->get();
        $certificadosVendidos = DB::connection('mysql')->table('cartelas_validas')->select('sequencial', 'cd_revendedor')->get();
        $certificadosDevolvidos = DB::connection('mysql')->table('cartelas_devolvidas')->select('sequencial', 'cd_revendedor')->get();
        return view('dashboards.resumo')->with(compact('title', 'distribuidor', 'certificados'));
    }

    public function devolucaoReal(){
        set_time_limit(1800);
        $title = "Devolução";
        $distribuidor = DB::connection('mysql')->table('a_revendedor')
        ->orderBy('NM_REVENDEDOR')
        ->get();

        
    }

}
