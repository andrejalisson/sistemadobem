<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PesquisaController extends Controller{

    public function certificado(){
        $title = "Pesquisa Certificado";
        return view('pesquisa.pesquisa')->with(compact( 'title'));
    }

    public function certificadoPost(Request $request){
        $title = "Pesquisa Certificado";
        if ($request->pesquisa > 300000) {
            $certificado = $request->pesquisa - 300000;
        }else{
            $certificado = $request->pesquisa;
        }
        $certificado = DB::connection('mysql')->table('cartelas_validas')
        ->join('a_revendedor', 'cartelas_validas.cd_revendedor', '=', 'a_revendedor.CD_REVENDEDOR')
        ->where('sequencial', $certificado)->get();
        return view('pesquisa.pesquisa')->with(compact('title', 'certificado'));
    }

}
