<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RegiaoController extends Controller{

    public function lista(){
        $title = "Regiões";
        return view('regioes.lista')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Região";
        return view('regioes.add')->with(compact('title'));
    }

    public function editar($id){
        $title = "Editar Região";
        $regiao = DB::table('regiao')->where('id_reg', $id)->first();
        return view('regioes.editar')->with(compact('title', 'regiao'));
    }

    public function addPost(Request $request){
        DB::table('regiao')->insert([
            'nome_reg' => $request->nome,
            'status_reg' => 1,
            ]
        );
        $request->session()->flash('sucesso', 'Região adicionada com sucesso!');
        return redirect('/Regioes/Listar');
    }

    public function editarPost(Request $request, $id){
        DB::table('regiao')
            ->where('id_reg', $id)
            ->update(['nome_reg' => $request->nome]);
        $request->session()->flash('sucesso', 'Região Editada com sucesso!');
        return redirect('/Regioes/Listar');
    }

    public function excluir(Request $request, $id){
        DB::table('regiao')
        ->where('id_reg', $id)
        ->update(['status_reg' => 0]);
        $request->session()->flash('sucesso', 'Região excluida com sucesso!');
        return redirect('/Regioes/Listar');
    }

    public function todasRegioes(Request $request){

        $columns = array(
            0 =>'id_reg',
            1 =>'nome_reg',
        );
        
        $totalData = DB::table('regiao')
                        ->where('status_reg', 1)
                        ->count();
        
        
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $regiao = DB::table('regiao')
                            ->where('status_reg', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            
            
        }
        else{
            $search = $request->input('search.value');
            $regiao =  DB::table('regiao')
                            ->where('nome_reg','ILIKE',"%{$search}%")
                            ->where('status_reg', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('regiao')
                            ->where('nome_reg','ILIKE',"%{$search}%")
                            ->where('status_reg', 1)
                            ->count();
            
        }
        $data = array();
        if(!empty($regiao)){
            foreach ($regiao as $regiao){
                $nestedData['id'] = "# ".$regiao->id_reg;
                $nestedData['regiao'] = $regiao->nome_reg;
                $view = "";
                $editar = "onclick=\"location.href='/Regioes/EditarRegiao/".$regiao->id_reg."'\"";
                $excluir = "onclick=\"location.href='/Regioes/ExcluirRegiao/".$regiao->id_reg."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\" ".$excluir. " type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }



}
