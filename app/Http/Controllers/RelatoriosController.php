<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RelatoriosController extends Controller{
    public function processamentoPost(Request $request){
        set_time_limit(1800);
        if ($request->distribuidor != 0) {
            $distribuidores = DB::table('distribuidor')
            ->where('id_dis', $request->distribuidor)
            ->where('id_dis','<>', 59)
            ->first();
            $nome = $distribuidores->nome_dis."-".$request->edicao.".pdf";
            $certificados = DB::table('certificados')->where('edicao_cer', $request->edicao)->where('distribuidor_id', $request->distribuidor)->get();
            $extraviados = DB::table('certificados')->where('edicao_cer', $request->edicao)->where('vendido_cer', false)->where('devolvido_cer', false)->where('distribuidor_id', $request->distribuidor)->get();
            $edicao = $request->edicao;
            return \PDF::loadView('relatorios.teste', compact('distribuidores', 'certificados', 'extraviados', 'edicao'))
                        ->download($nome);
        }else {
            $distribuidores = DB::table('distribuidor')
            ->where('id_dis','<>', 59)
            ->get();
            $certificados = DB::table('certificados')->where('edicao_cer', $request->edicao)->where('distribuidor_id','<>',59)->get();
            return \PDF::loadView('relatorios.geral', compact('distribuidores','certificados'))
                        //->setPaper('a4', 'landscape')
                        ->download('Geral-'.$request->edicao.'.pdf');
        }
        
    }

    public function processamento(){
        $title = "Relatório Processamento";
        $distribuidor = DB::table('distribuidor')->where('status_dis', true)->get();
        $edicoes = DB::table('edicao')->where('status_edc', true)->orderBy('id_edc', 'desc')->get();
        return view('relatorios.processamento')->with(compact('title', 'distribuidor', 'edicoes'));
    }

    public function extravioResumo(){
        $title = "Relatório Resumido Extravio";
        $edicoes = DB::table('edicao')->where('status_edc', true)->get();
        return view('relatorios.resumoExtravio')->with(compact('title', 'edicoes'));
    }

    public function extravioResumoPost(Request $request){
        set_time_limit(1800);
            $certificados = DB::table('certificados')
            ->selectRaw('distribuidor_id, count(numero_cer) AS `extravio`')
            ->where('distribuidor_id','<>', 59)
            ->where('edicao_cer', 10)
            ->where('vendido_cer', false)->where('devolvido_cer', false)
            ->groupBy('distribuidor_id')
            ->orderBy('extravio','DESC')
            ->get();
            $distribuidor = DB::table('distribuidor')
            ->get();
            return \PDF::loadView('relatorios.resumoE', compact('certificados', 'distribuidor'))
                        //->setPaper('a4', 'landscape')
                        ->download('Extravio-Geral-'.$request->edicao.'.pdf');
        
    }

}
