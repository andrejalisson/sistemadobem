<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AcessoController extends Controller{
    public function login(){
        $title = "Login";
        return view('acesso.login')->with(compact('title'));
    }

    public function forgot(){
        $title = "Esqueceu a senha?";
        return view('acesso.forgot')->with(compact('title'));
    }

    public function forgotPost(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('usarname_user', $request->usuario)->first();
        if($usuario != null){
            $request->session()->flash('sucesso', 'Um email foi enviado com as instruções!');
            return redirect('/');
        }else{
            $request->session()->flash('alerta', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

    public function verifica(Request $request){
        $usuario = DB::table('usuarios')->where('email_user', $request->usuario)->orwhere('usarname_user', $request->usuario)->first();
        if($usuario != null){
            if(password_verify($request->senha, $usuario->senha_user )){
                $request->session()->flash('sucesso', 'Bom trabalho!');
                $request->session()->put('id', $usuario->id_user);
                $request->session()->put('nome', $usuario->nome_user);
                $request->session()->put('login', $usuario->usarname_user);
                $request->session()->put('logado', true);
                return redirect('/Regioes/Listar');
            }else{
                $request->session()->flash('alerta', 'Senha incorreta!');
                return redirect()->back();
            }
        }else{
            $request->session()->flash('alerta', 'Usuário não encontrado.');
            return redirect()->back();
        }
    }

}
