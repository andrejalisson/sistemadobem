<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VendedorController extends Controller{

    public function lista(){
        $title = "Vendedor";
        return view('vendedor.lista')->with(compact('title'));
    }

    public function add(){
        $distribuidor = DB::table('distribuidor')->where('status_dis', 1)->get();
        $title = "Adicionar Vendedor";
        return view('vendedor.add')->with(compact('title', 'distribuidor'));
    }

    public function addPost(Request $request){
        DB::table('vendedor')->insert([
            'nome_ven' => $request->nome,
            'responsavel_ven' => $request->responsavel,
            'telefone_ven' => $request->telefone,
            'regiaoId_ven' => $request->regiao,

            'tipo_ven' => $request->tipo,

            'enderecoC_ven' => $request->enderecoC,
            'bairroC_ven' => $request->bairroC,
            'cidadeC_ven' => $request->cidadeC,
            'referenciaC_ven' => $request->referenciaC,

            'enderecoR_ven' => $request->enderecoR,
            'bairroR_ven' => $request->bairroR,
            'cidadeR_ven' => $request->cidadeR,
            'referenciaR_ven' => $request->referenciaR,

//            'cadastro_ven'

            'fechamento_ven' => $request->fechamento,

//            'distribuidorId_ven'

            'status_ven' => true,
            ]
        );
        $request->session()->flash('sucesso', 'Vendedor adicionado com sucesso!');
        return redirect('/Vendedores/Listar');
    }

    //public function todosVendedores(Request $request){
        
    //}

//    public function editar($id){
        
//    }

//    public function editarPost(Request $request, $id){

//    }

//    public function migrar(){

//        return redirect('/Vendedores/Listar');
//    }  

}
