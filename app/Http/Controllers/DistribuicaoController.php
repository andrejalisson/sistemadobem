<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DistribuicaoController extends Controller{

    public function listar(){
        $distribuidor = DB::table('distribuidor')->where('status_dis', 1)->get();
        $edicao = DB::table('edicao')->where('status_edc', 1)->orderBy('sorteio_edc', 'desc')->get();
        $title = "Distribuição Distribuidor";
        return view('distribuicao.lista')->with(compact('title', 'distribuidor', 'edicao'));
    }

    public function add(){
        $distribuidor = DB::table('distribuidor')->where('status_dis', 1)->get();
        $edicao = DB::table('edicao')->where('status_edc', 1)->orderBy('sorteio_edc', 'desc')->get();
        $title = "Adicionar Distribuição Manual";
        return view('distribuicao.add')->with(compact('title', 'distribuidor', 'edicao'));
    }

    public function addpost(Request $request){
        $inicial = str_replace('.', '', $request->inicial);
        $final = str_replace('.', '', $request->final);
        $total = $final - $inicial +1;
        DB::table('certificados')
            ->where('numero_cer', '>=', $inicial)
            ->where('numero_cer', '<=', $final)
            ->where('edicao_cer',  intval($request->edc))
            ->update(['distribuidor_id' => $request->distribuidor, 'status_cer' => true]
        );
            
        
        DB::table('distribuicao')->insert([
            'edicao_dist' => $request->edc,
            'distribuidor_dist' => $request->distribuidor,
            'inicial_dist' => $inicial,
            'final_dist' => $final,
            'liberacao_dist' => date('Y-m-d'),
            ]
        );
        return $total;
    }



    public function todasDistribuicoesDistribuidores(Request $request){
        $columns = array(
            0 =>'id_dist',
            1 =>'edicao_dist',
            2 =>'distribuidor_dist',
            3 =>'inicial_dist',
            4 =>'final_dist',
        );
        
        $totalData = DB::table('distribuicao')
                        ->Join('edicao', 'numero_edc', '=', 'edicao_dist')
                        ->whereDate('sorteio_edc', '>=', date('Y-m-d'))
                        ->count();
        
        
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $distribuicao = DB::table('distribuicao')
                            ->join('distribuidor', 'distribuidor_dist', '=', 'id_dis')
                            ->Join('edicao', 'numero_edc', '=', 'edicao_dist')
                            ->whereDate('sorteio_edc', '>=', date('Y-m-d'))
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
            
            
        }
        else{
            $search = $request->input('search.value');
            $distribuicao =  DB::table('distribuicao')
                            ->join('distribuidor', 'distribuidor_dist', '=', 'id_dis')
                            ->Join('edicao', 'numero_edc', '=', 'edicao_dist')
                            // ->whereDate('sorteio_edc', '>=', date('Y-m-d'))
                            ->where('nome_dis','LIKE',"%{$search}%")
                            ->orWhere('edicao_dist','LIKE',"%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('distribuicao')
                            ->join('distribuidor', 'distribuidor_dist', '=', 'id_dis')
                            ->Join('edicao', 'numero_edc', '=', 'edicao_dist')
                            // ->whereDate('sorteio_edc', '>=', date('Y-m-d'))
                            ->where('nome_dis','LIKE',"%{$search}%")
                            ->orWhere('edicao_dist','LIKE',"%{$search}%")
                            ->count();
            
        }
        $data = array();
        if(!empty($distribuicao)){
            foreach ($distribuicao as $distribuicao){
                $nestedData['id'] = "# ".$distribuicao->id_dist;
                $nestedData['edicao'] = "# ".$distribuicao->edicao_dist;
                $nestedData['distribuidor'] = $distribuicao->nome_dis;
                switch (strlen($distribuicao->inicial_dist)) {
                    case 1:
                        $inicial = $complementoInicial = "000.00".$distribuicao->inicial_dist;
                        break;
                    case 2:
                        $inicial = $complementoInicial = "000.0".$distribuicao->inicial_dist;
                        break;
                    case 3:
                        $inicial = $complementoInicial = "000.".$distribuicao->inicial_dist;
                        break;
                    case 4:
                        $inicial = $complementoInicial = "00".substr($distribuicao->inicial_dist, -4, 1).".".substr($distribuicao->inicial_dist, -3);
                        break;
                    case 5:
                        $inicial = $complementoInicial = "0".substr($distribuicao->inicial_dist, -5, 2).".".substr($distribuicao->inicial_dist,-3);
                        break;
                    
                    default:
                        $inicial = substr($distribuicao->inicial_dist, -6, 3).".".substr($distribuicao->inicial_dist, -3);
                        break;
                }

                switch (strlen($distribuicao->final_dist)) {
                    case 1:
                        $final = $complementoFinal = "000.00".$distribuicao->final_dist;
                        break;
                    case 2:
                        $final = $complementoFinal = "000.0".$distribuicao->final_dist;
                        break;
                    case 3:
                        $final = $complementoFinal = "000.".$distribuicao->final_dist;
                        break;
                    case 4:
                        $final = $complementoFinal = "00".substr($distribuicao->final_dist, -4, 1).".".substr($distribuicao->final_dist, -3);
                        break;
                    case 5:
                        $final = $complementoFinal = "0".substr($distribuicao->final_dist, -5, 2).".".substr($distribuicao->final_dist,-3);
                        break;
                    
                    default:
                        $final = substr($distribuicao->final_dist, -6, 3).".".substr($distribuicao->final_dist, -3);
                        break;
                }
                $nestedData['inicial'] = $inicial;
                $nestedData['final'] = $final;
                $nestedData['total'] = $distribuicao->final_dist-$distribuicao->inicial_dist+1;
                $view = "";
                $editar = "onclick=\"location.href='/Distribuidores/Editar/".$distribuicao->id_dist."'\"";
                $excluir = "onclick=\"location.href='/Regioes/ExcluirRegiao/".$distribuicao->id_dist."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\"  type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
