<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MasterController extends Controller{
    
    public function lista(){
        $title = "Distribuidores Master";
        return view('master.lista')->with(compact('title'));
    }

    public function add(){
        $regiao = DB::table('regiao')->where('status_reg', 1)->get();
        $title = 'Adicionar Master';
        return view('master.add')->with(compact('title', 'regiao'));
    }

    public function addPost(Request $request){
        $telefone = str_replace('(', '', $request->telefone);
        $telefone = str_replace(')', '', $telefone);
        $telefone = str_replace('-', '', $telefone);
        $telefone = str_replace(' ', '', $telefone);
        DB::table('master')->insert([
            'nome_mas' => $request->nome,
            'responsavel_mas' => $request->responsavel,
            'telefone_mas' => $telefone,
            'regiaoId_mas' => $request->regiao,
            'status_mas' => true,
            ]
        );
        $request->session()->flash('sucesso', 'Master adicionado com sucesso!');
        return redirect('/Master/Listar');
    }

    public function todosMaster(Request $request){
        $columns = array(
            0 =>'nome_mas',
            1 =>'responsavel_mas',
            2 =>'telefone_mas',
        );
        
        $totalData = DB::table('master')
                        ->where('status_mas', 1)
                        ->count();
        
        
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            $master = DB::table('master')
                            ->where('status_mas', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $master =  DB::table('master')
                            ->where('nome_mas','LIKE',"%{$search}%")
                            ->orWhere('responsavel_mas','LIKE',"%{$search}%")
                            ->orWhere('telefone_mas','LIKE',"%{$search}%")
                            ->where('status_mas', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('master')
                            ->where('nome_mas','LIKE',"%{$search}%")
                            ->orWhere('responsavel_mas','LIKE',"%{$search}%")
                            ->orWhere('telefone_mas','LIKE',"%{$search}%")
                            ->where('status_mas', 1)
                            ->count();
        }

        $data = array();
        
        if(!empty($master)){
            foreach ($master as $master){
                $nestedData['nome'] = $master->nome_mas;
                $nestedData['responsavel'] = $master->responsavel_mas;
                $nestedData['telefone'] = $master->telefone_mas;
                $view = "";
                $editar = "onclick=\"location.href='/Distribuidores/Editar/".$master->id_mas."'\"";
                $excluir = "onclick=\"location.href='/Regioes/ExcluirRegiao/".$master->id_mas."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-primary btn-circle\"  type=\"button\"><i class=\"far fa-eye\"></i></button>
                                            <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\"  type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
