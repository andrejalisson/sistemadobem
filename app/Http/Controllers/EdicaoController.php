<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EdicaoController extends Controller{

    public function listar(){
        $title = "Edição";
        return view('edicao.lista')->with(compact('title'));
    }

    public function certificados(){
        $title = "Certificados";
        return view('edicao.certificados')->with(compact('title'));
    }

    public function addCertificados(){
        $title = "Adicionar Tipo de Certificados";
        return view('edicao.addCertificados')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Edição";
        return view('edicao.add')->with(compact('title'));
    }

    public function addPost(Request $request){
        Echo "<h1>Processo Iniciado</h1>";
        $sorteio = str_replace('/', '-', $request->sorteio);
        $sorteio = date('Y-m-d', strtotime($sorteio));
        $liberacao = str_replace('/', '-', $request->liberacao);
        $liberacao = date('Y-m-d', strtotime($liberacao));
        
        DB::table('edicao')->insert([
            'numero_edc' => $request->edicao,
            'sorteio_edc' => $sorteio,
            'liberacao_edc' => $liberacao,
            'globo_edc' => $request->globo,
            'giro_edc' => $request->giro,
            'dupla_edc' => 1,
            'saltoDupla_edc' => 300000,
            'qtdCertificados_edc' => str_replace(".", "", $request->quantidade),
            'tipoCertificados_edc' => 4,
            'status_edc' => 1,
            ]
        );
        Echo "<h1>Edição Criada</h1>";
        $totall = str_replace(".", "", $request->quantidade);
        $total = $totall / 4;
        $ceara10 = 45000;
        $ceara2 = 12000;
        $fortaleza10 = 90000;
        $fortaleza2 = 150000;
        $inicial = $request->inicial;
        while ($inicial <= $ceara10) { 
            DB::table('certificados')->insert([
                'numero_cer' => $inicial,
                'edicao_cer' => $request->edicao,
                'valor_cer' => 10,
                'distribuidor_id' => 0,
                'vendedor_id' => 0,
                'status_cer' => 0,
                ]
            );
            Echo "<h5>".$inicial." - Ceará R$10,00 OK</h5>";
            $inicial++;
            
        }
        Echo "<h1>Certificado Ceará R$10,00 OK</h1>";
        while ($inicial <= $fortaleza10) {       
            DB::table('certificados')->insert([
                'numero_cer' => $inicial,
                'edicao_cer' => $request->edicao,
                'valor_cer' => 10,
                'distribuidor_id' => 0,
                'vendedor_id' => 0,
                'status_cer' => 0,
                ]
            );
            Echo "<h5>".$inicial." - Fortaleza R$10,00 OK</h5>";
            $inicial++;
            
        }
        Echo "<h1>Certificado Fortaleza R$10,00 OK</h1>";
        while ($inicial <= $ceara2) {         
            DB::table('certificados')->insert([
                'numero_cer' => $inicial,
                'edicao_cer' => $request->edicao,
                'valor_cer' => 2,
                'distribuidor_id' => 0,
                'vendedor_id' => 0,
                'status_cer' => 0,
                ]
            );
            Echo "<h5>".$inicial." - Ceará R$2,00 OK</h5>";
            $inicial++;
            
        }
        Echo "<h1>Certificado Ceará R$2,00 OK</h1>";
        while ($inicial <= $fortaleza2) {     
            DB::table('certificados')->insert([
                'numero_cer' => $inicial,
                'edicao_cer' => $request->edicao,
                'valor_cer' => 2,
                'distribuidor_id' => 0,
                'vendedor_id' => 0,
                'status_cer' => 0,
                ]
            );
            Echo "<h5>".$inicial." - Fortaleza R$2,00 OK</h5>";
            $inicial++;
            
        }
        Echo "<h1>Certificado Fortaleza R$2,00 OK</h1>";
        return redirect('/Edicao/listar');
    }


    public function todosCertificados(Request $request){
        $columns = array(
            0 =>'id_tcer',
            1 =>'nome_tcer',
        );
        $totalData = DB::table('tipo_certificado')
                        ->where('status_tcer', 1)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $certificado = DB::table('tipo_certificado')
                            ->where('status_tcer', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $certificado =  DB::table('tipo_certificado')
                            ->where('numero_edc','ILIKE',"%{$search}%")
                            ->where('status_tcer', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('tipo_certificado')
                            ->where('nome_tcer','ILIKE',"%{$search}%")
                            ->where('status_tcer', 1)
                            ->count();
        }
        $data = array();
        if(!empty($certificado)){
            foreach ($certificado as $certificado){
                $nestedData['id'] = "# ".$certificado->id_tcer;
                $nestedData['nome'] = $certificado->nome_tcer;
                $view = "";
                $editar = "onclick=\"location.href='/Regioes/Editaredicao/".$certificado->id_tcer."'\"";
                $excluir = "onclick=\"location.href='/Regioes/Excluiredicao/".$certificado->id_tcer."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-primary btn-circle\"  type=\"button\"><i class=\"far fa-eye\"></i></button>
                                            <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\"  type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }


    public function todasEdicoes(Request $request){

        $columns = array(
            0 =>'numero_edc',
            1 =>'qtdCertificados_edc',
            2 =>'globo_edc',
            3 =>'giro_edc',
            4 =>'sorteio_edc',
            5 =>'liberacao_edc',
        );
        $totalData = DB::table('edicao')
                        ->where('status_edc', 1)
                        ->count();
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $edicao = DB::table('edicao')
                            ->where('status_edc', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $edicao =  DB::table('edicao')
                            ->where('numero_edc','ILIKE',"%{$search}%")
                            ->where('status_edc', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('edicao')
                            ->where('numero_edc','ILIKE',"%{$search}%")
                            ->where('status_edc', 1)
                            ->count();
        }
        $data = array();
        if(!empty($edicao)){
            foreach ($edicao as $edicao){
                $nestedData['edicao'] = "# ".$edicao->numero_edc;
                $nestedData['certificados'] = $edicao->qtdCertificados_edc;
                $nestedData['globo'] = $edicao->globo_edc;
                $nestedData['giro'] = $edicao->giro_edc;
                $nestedData['sorteio'] = date('d/m/Y', strtotime($edicao->sorteio_edc));
                $nestedData['liberacao'] = date('d/m/Y', strtotime($edicao->liberacao_edc));
                $view = "";
                $editar = "onclick=\"location.href='/Regioes/Editaredicao/".$edicao->id_edc."'\"";
                $excluir = "onclick=\"location.href='/Regioes/Excluiredicao/".$edicao->id_edc."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-primary btn-circle\"  type=\"button\"><i class=\"far fa-eye\"></i></button>
                                            <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\"  type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
