<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsuariosController extends Controller{
    
    public function listar(){
        $title = "Usuários";
        return view('usuarios.lista')->with(compact('title'));
    }

    public function add(){
        $title = "Adicionar Usuário";
        return view('usuarios.add')->with(compact('title'));
    }

    public function editar($id){
        $title = "Editar Usuário";
        $usuario = DB::table('usuarios')->where('id_user', $id)->get();
        return view('usuarios.edt')->with(compact('title', 'usuario'));
    }

    public function editarPost(Request $request){
        if ($request->senha1 == "") {
            DB::table('usuarios')
            ->where('id_user', $request->id_user)
            ->update([
                'nome_user' => $request->nome,
                'email_user' => $request->email,
                'CPF_user' => preg_replace("/[^0-9]/", "", $request->cpf),
                'usarname_user' => $request->usuario,
                'alteracao_user' => date("Y-m-d"),
                'status_user' => 1,
                ]);
                $request->session()->flash('sucesso', 'Usuário Editado com Sucesso!');
                return redirect('/Usuarios/Listar');
        } else {
            if ($request->senha1 == $request->senha2) {
                DB::table('usuarios')
                ->where('id_user', $request->id_user)
                ->update([
                    'nome_user' => $request->nome,
                    'email_user' => $request->email,
                    'senha_user' => password_hash($request->senha1, PASSWORD_BCRYPT),
                    'CPF_user' => preg_replace("/[^0-9]/", "", $request->cpf),
                    'usarname_user' => $request->usuario,
                    'alteracao_user' => date("Y-m-d"),
                    'status_user' => 1,
                    ]);
                $request->session()->flash('sucesso', 'Usuário Editado com Sucesso!');
                return redirect('/Usuarios/Listar');
            }else{
                $request->session()->flash('alerta', 'A senha não confere.');
                return redirect()->back();
            }
        }
    }

    public function excluir($id, Request $request){
        DB::table('usuarios')
        ->where('id_user', $id)
        ->update(['status_user' => 0]);
        $request->session()->flash('sucesso', 'Usuário Excluído com sucesso!');
        return redirect('/Usuarios/Listar');
    }

    public function ativar($id, Request $request){
        DB::table('usuarios')
        ->where('id_user', $id)
        ->update(['status_user' => 1]);
        $request->session()->flash('sucesso', 'Usuário Ativado com sucesso!');
        return redirect('/Usuarios/Listar');
    }

    public function addPost(Request $request){
        if ($request->senha1 == $request->senha2) {
            DB::table('usuarios')->insert([
                'nome_user' => $request->nome,
                'senha_user' => password_hash($request->senha1, PASSWORD_BCRYPT),
                'email_user' => $request->email,
                'CPF_user' => preg_replace("/[^0-9]/", "", $request->cpf),
                'usarname_user' => $request->usuario,
                'criacao_user' => date("Y-m-d"),
                'alteracao_user' => date("Y-m-d"),
                'status_user' => 1,
                ]
            );
            
            $request->session()->flash('sucesso', 'Usuário Adicionado com sucesso!');
            return redirect('/Usuarios/Listar');
        }else{
            $request->session()->flash('alerta', 'A senha não confere.');
            return redirect()->back();
        }
    }



    public function todosUsuarios(Request $request){

        $columns = array(
            0 =>'id_user',
            1 =>'nome_user',
            2 =>'email_user',
            3 =>'CPF_user',
            4 =>'usarname_user',
        );
        
        $totalData = DB::table('usuarios')
                        ->where('status_user', 1)
                        ->count();
        
        
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        if(empty($request->input('search.value'))){
            $usuarios = DB::table('usuarios')
                            ->where('status_user', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
        }
        else{
            $search = $request->input('search.value');
            $usuarios =  DB::table('usuarios')
                            ->where('nome_user','ILIKE',"%{$search}%")
                            ->orwhere('email_user','ILIKE',"%{$search}%")
                            ->orwhere('CPF_user','ILIKE',"%{$search}%")
                            ->orwhere('usarname_user','ILIKE',"%{$search}%")
                            ->where('status_user', 1)
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order,$dir)
                            ->get();
                            
            $totalFiltered = DB::table('usuarios')
                            ->where('nome_user','ILIKE',"%{$search}%")
                            ->orwhere('email_user','ILIKE',"%{$search}%")
                            ->orwhere('CPF_user','ILIKE',"%{$search}%")
                            ->orwhere('usarname_user','ILIKE',"%{$search}%")
                            ->where('status_reg', 1)
                            ->count();
        }
        $data = array();
        if(!empty($usuarios)){
            foreach ($usuarios as $usuarios){
                $nestedData['id'] = "# ".$usuarios->id_user;
                $nestedData['nome'] = $usuarios->nome_user;
                $nestedData['email'] = $usuarios->email_user;
                $nestedData['username'] = $usuarios->usarname_user;
                $nestedData['cpf'] = $usuarios->CPF_user;
                $view = "";
                $editar = "onclick=\"location.href='/Usuarios/Editar/".$usuarios->id_user."'\"";
                $excluir = "onclick=\"location.href='/Usuarios/Excluir/".$usuarios->id_user."'\"";
                $nestedData['opcoes'] = "   <button class=\"btn btn-primary btn-circle\"  type=\"button\"><i class=\"far fa-eye\"></i></button>
                                            <button class=\"btn btn-warning btn-circle\" ".$editar. " type=\"button\"><i class=\"fas fa-pen\"></i></button>
                                            <button class=\"btn btn-danger btn-circle\" ".$excluir. " type=\"button\"><i class=\"far fa-times-circle\"></i></button>";
                $data[] = $nestedData;
            }
        }
        $json_data = array(
                    "draw"            => intval($request->input('draw')),
                    "recordsTotal"    => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data"            => $data
                    );
        echo json_encode($json_data);
    }
}
