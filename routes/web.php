<?php
//ACESSO
Route::get('/', 'AcessoController@login');
Route::get('/Login', 'AcessoController@login');
Route::post('/Login', 'AcessoController@verifica');
Route::get('/RedefinirSenha', 'AcessoController@forgot');
Route::post('/RedefinirSenha', 'AcessoController@forgotPost');

Route::group(['middleware' => ['VerificaAutenticacao']], function() {
    //DashBoard
    Route::get('/Dashboard/Devolucao', 'DashboardController@devolucao');
    Route::get('/Dashboard/Resumo', 'DashboardController@resumo');
    Route::get('/Dashboard/DevolucaoReal', 'DashboardController@devolucaoReal');
    Route::post('/Pesquisa', 'PesquisaController@certificadoPost');

    Route::get('/Dashboards/Vendas', 'DashboardController@contagemVendas');
    Route::get('/Dashboards/Vendas/{id}', 'DashboardController@contagemVendasDistribuidor');

    // Route::get('/Dashboards/Vendas', 'DashboardController@financeiro');

    //REGIOES
    Route::get('/Regioes/Listar', 'RegiaoController@lista');
    Route::get('/Regioes/AdicionarRegioes', 'RegiaoController@add');
    Route::post('/AdicionarRegioes', 'RegiaoController@addPost');
    Route::post('/todasRegioes', 'RegiaoController@todasRegioes')->name('todasRegioes');
    Route::get('/Regioes/EditarRegiao/{id}', 'RegiaoController@editar');
    Route::post('/EditarRegiao/{id}', 'RegiaoController@editarPost');
    Route::get('/Regioes/ExcluirRegiao/{id}', 'RegiaoController@excluir');

    //DISTRIBUIDOR MASTER
    Route::get('/Master/Listar', 'MasterController@lista');
    Route::get('/Master/Adicionar', 'MasterController@add');
    Route::post('/Master/Adicionar', 'MasterController@addPost');
    Route::post('/todosMaster', 'MasterController@todosMaster')->name('todosMaster');
    Route::get('/Master/Editar/{id}', 'MasterController@editar');
    Route::post('/Master/Editar/{id}', 'MasterController@editarPost');
    Route::get('/Master/Migrar', 'MasterController@migrar');   
    
    //DISTRIBUIDORES
    Route::get('/Distribuidores/Listar', 'DistribuidorController@lista');
    Route::get('/Distribuidores/Adicionar', 'DistribuidorController@add');
    Route::post('/Distribuidores/Adicionar', 'DistribuidorController@addPost');
    Route::post('/todosDistribuidores', 'DistribuidorController@todosDistribuidores')->name('todosDistribuidores');
    Route::get('/Distribuidores/Editar/{id}', 'DistribuidorController@editar');
    Route::post('/Distribuidores/Editar/{id}', 'DistribuidorController@editarPost');
    Route::get('/Distribuidores/Excluir/{id}', 'DistribuidorController@excluir');

    //VENDEDORES
    Route::get('/Vendedores/Listar', 'VendedorController@lista');
    Route::get('/Vendedores/Adicionar', 'VendedorController@add');
    Route::post('/Vendedores/Adicionar', 'VendedorController@addPost');
    Route::post('/todosVendedores', 'VendedorController@todosVendedores')->name('todosVendedores');
    Route::get('/Vendedores/Editar/{id}', 'VendedorController@editar');
    Route::post('/Vendedores/Editar/{id}', 'VendedorController@editarPost');
    Route::get('/Vendedores/Migrar', 'VendedorController@migrar');

    // //DISTRIBUIDORES
    // Route::get('/Usuarios/Listar', 'DistribuidorController@lista');
    // Route::get('/Usuarios/Adicionar', 'DistribuidorController@add');
    // Route::post('/Usuarios/Adicionar', 'DistribuidorController@addPost');
    // Route::post('/todosUsuarios', 'DistribuidorController@todosDistribuidores')->name('todosDistribuidores');
    // Route::get('/Usuarios/Editar/{id}', 'DistribuidorController@editar');
    // Route::post('/Usuarios/Editar/{id}', 'DistribuidorController@editarPost');
    // Route::get('/Usuarios/Migrar', 'DistribuidorController@migrar');

    //USUÁRIOS
    Route::get('/Usuarios/Listar', 'UsuariosController@listar');
    Route::get('/Usuarios/Adicionar', 'UsuariosController@add');
    Route::post('/Usuarios/Adicionar', 'UsuariosController@addPost');
    Route::post('/todosUsuarios', 'UsuariosController@todosUsuarios')->name('todosUsuarios');
    Route::get('/Usuarios/Editar/{id}', 'UsuariosController@editar');
    Route::post('/Usuarios/Editar/{id}', 'UsuariosController@editarPost');
    Route::get('/Usuarios/Excluir/{id}', 'UsuariosController@excluir');
    Route::get('/Usuarios/Ativar/{id}', 'UsuariosController@ativar');

    //PROCESSAMENTO
    Route::get('/Processamento/DevolucaoIGS', 'ProcessamentoController@devolucaoIGS');
    Route::get('/Processamento/DevolucaoBIP', 'ProcessamentoController@devolucaoBIP');
    Route::get('/Processamento/ContagemVenda', 'ProcessamentoController@contagemVenda');
    Route::get('/Processamento/ContagemVendaSecundario', 'ProcessamentoController@contagemVendaSecundario');
    Route::post('/Processamento/ContagemVendaSecundario', 'ProcessamentoController@contagemVendaSecundarioPost');
    Route::post('/Processamento/Devolucao', 'ProcessamentoController@devolucao');
    Route::post('/Processamento/DevolucaoBIP', 'ProcessamentoController@devolucaoPostBip');
    Route::post('/Processamento/ContagemVenda', 'ProcessamentoController@contagemVendaPost');

    Route::get('/Processamento/ProcessarDistribuicao/{id}', 'ProcessamentoController@atualizar');

    Route::get('/Processamento/adddda', 'ProcessamentoController@carregarCodigo');

    Route::get('/Processamento/Atualizar', 'ProcessamentoController@atualizar');

    //EDIÇÃO
    Route::get('/Edicao/listar', 'EdicaoController@listar');
    Route::get('/Edicao/Certificados', 'EdicaoController@certificados');
    Route::post('/todasEdicoes', 'EdicaoController@todasEdicoes')->name('todasEdicoes');
    Route::post('/todosCertificados', 'EdicaoController@todosCertificados')->name('todosCertificados');
    Route::get('/Edicao/AdicionarEdicao', 'EdicaoController@add');
    Route::get('/Edicao/AdicionarTipoCertificado', 'EdicaoController@addCertificados');
    Route::post('/Edicao/AdicionarEdicao', 'EdicaoController@addpost');

    //DISTRIBUIÇÃO
    Route::get('/Distribuicao/listar', 'DistribuicaoController@listar');
    Route::post('/todasDistribuicoesDistribuidores', 'DistribuicaoController@todasDistribuicoesDistribuidores')->name('todasDistribuicoesDistribuidores');
    Route::post('/Distribuicao/AdicionarDistribuicao', 'DistribuicaoController@addpost');
    Route::get('/Distribuicao/Estoque', 'DistribuicaoController@estoque');
    Route::post('/Distribuicao/Estoque', 'DistribuicaoController@estoquePost');


    //RELATORIOS
    Route::get('/Relatorio/teste', 'RelatoriosController@teste');
    Route::get('/Relatorio/Processamento', 'RelatoriosController@processamento');
    Route::post('/Relatorio/Processamento', 'RelatoriosController@processamentoPost');
    Route::get('/Relatorio/ExtravioResumo', 'RelatoriosController@extravioResumo');
    Route::post('/Relatorio/ExtravioResumo', 'RelatoriosController@extravioResumoPost');


    //FINANCEIRO
    Route::get('/Financeiro/Contas', 'FinanceiroController@contas');
    Route::post('/todasContas', 'FinanceiroController@todasContas')->name('todasContas');

    //MATRIZ
    Route::get('/Matriz/Listar', 'MatrizController@lista');
    
});
Route::get('/Matriz/Gerar', 'MatrizController@gerarMatriz');






