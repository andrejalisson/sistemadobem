@extends('templates.admin')

@section('css')
<link href="/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" action="/Edicao/AdicionarEdicao">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-sm-1">
                            <label>Edição</label>
                            <input type="text" data-mask="999" required name="edicao" class="form-control" placeholder="edição">
                        </div>
                        <div class="col-sm-2">
                            <label>Giro da Sorte</label>
                            <input type="text" data-mask="99" required name="giro" class="form-control" placeholder="Quatidades">
                        </div>
                        <div class="col-sm-2">
                            <label>Globo</label>
                            <input type="text" data-mask="99" required name="globo" class="form-control" placeholder="Quatidades">
                        </div>
                        <div class="col-sm-2">
                            <label>Qtd Certificados</label>
                            <input type="text" data-mask="999.999" required name="quantidade" class="form-control" placeholder="Quatidades">
                        </div>
                        <div class="col-sm-1">
                            <label>Numero Inicial</label>
                            <input type="text" required name="inicial" class="form-control" placeholder="001">
                        </div>
                        <div class="col-sm-2" id="data_1">
                            <label>Data do Sorteio</label>
                            <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" value="{{date('d/m/Y', strtotime("next Sunday"))}}" required name="sorteio" class="form-control"  placeholder="Data do Sorteio">
                            </div>
                        </div>
                        <div class="col-sm-2" id="data_1">
                            <label>Data da Liberação</label>
                            <div class="input-group date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" value="{{ date('d/m/Y') }}" required name="liberacao" class="form-control"  placeholder="Data da Liberação">
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="/js/plugins/datapicker/bootstrap-datepicker.js"></script>
@endsection

@section('script')
<script>
$.fn.datepicker.dates['en'] = {
    days: ["Domingo", "Segunda", "Terça", "Wednesday", "Thursday", "Friday", "Saturday"],
    daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
    daysMin: ["Do", "Se", "Te", "Qu", "Qu", "Se", "Sa"],
    months: ["Janeiro", "Fevereiro", "Março", "Abri", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
    monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
    today: "Hoje",
    clear: "Limpar",
    format: "mm/dd/yyyy",
    titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
    weekStart: 0
};
    
    $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        format: 'dd/mm/yyyy',
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    	
</script>

@endsection