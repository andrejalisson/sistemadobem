<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Relatório</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-smfirst;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-smfirst;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>
  <table width="100%">
    <tr>
        <td valign="top"><img src="http://bemdagente.com/wp-content/uploads/2019/09/logo-150.png" width="150px" alt="100px"></td>
        <td align="right">
            <pre align="right">
              Edição:<strong> 011<strong>
              Data:{{" ".date('d/m/Y')}}
              Hora:{{" ".date('H:i:s')}}
              .
          </pre>
        </td>
    </tr>
  </table>
  @php
      $pagina = 0;
  @endphp
  
  @foreach ($distribuidores as $distribuidores)
        @php
            $distribuidosA =  $certificados->where('distribuidor_id', $distribuidores->id_dis)->count();
            $vendidosA =      $certificados->where('distribuidor_id', $distribuidores->id_dis)->where('vendido_cer', true)->count();
            $devolvidosA =    $certificados->where('distribuidor_id', $distribuidores->id_dis)->where('devolvido_cer', true)->count();
        @endphp
        
      @if ($certificados->where('distribuidor_id', $distribuidores->id_dis)->count() > 0)
          
      
        <h5>{{$distribuidores->nome_dis}}</h5>
        <table width="100%">
          <thead style="background-color: lightgray;">
            <tr>
              <th>Certificado</th>
              <th>Distribuido</th>
              <th>Vendido</th>
              <th>Devolvido</th>
              <th>Extravio</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th>Primário</th>
              <td>{{ $distribuidosA }}</td>
              <td>{{ $vendidosA }}</td>
              <td>{{ $devolvidosA }}</td>
              <td align="right">{{ $distribuidosA-$vendidosA-$devolvidosA }}</td>
            </tr>
          </tbody>

          <tfoot>
            <tr>
              <th class="gray">Totais</th>
              <td class="gray">{{ $certificados->where('distribuidor_id', $distribuidores->id_dis)->count() }}</td>
              <td class="gray">{{ $certificados->where('distribuidor_id', $distribuidores->id_dis)->where('vendido_cer', true)->count() }}</td>
              <td class="gray">{{ $certificados->where('distribuidor_id', $distribuidores->id_dis)->where('devolvido_cer', true)->count() }}</td>
              <td align="right" class="gray">{{ ($certificados->where('distribuidor_id', $distribuidores->id_dis)->count())-($certificados->where('distribuidor_id', $distribuidores->id_dis)->where('vendido_cer', true)->count())-($certificados->where('distribuidor_id', $distribuidores->id_dis)->where('devolvido_cer', true)->count()) }}</td>
            </tr>
          </tfoot>
        </table>
        @php
            $pagina++;
        @endphp
        @if ($pagina == 5)
          <div style='page-break-after: always;'></div>
          @php
              $paginas = 1;
          @endphp
        @else
          @if ($pagina > 5)
            @if ($paginas%6 == 0)
              <div style='page-break-after: always;'></div>
            @endif
            @php
                $paginas++;
            @endphp
          @endif
        @endif
      @endif
  @endforeach
  <h2><strong>Total Geral</strong></h2>
  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th style="border: 1px double black">Certificado</th>
        <th style="border: 1px double black">Distribuido</th>
        <th style="border: 1px double black">Vendido</th>
        <th style="border: 1px double black">Devolvido</th>
        <th style="border: 1px double black">Extravio</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th style="border: 1px double black">Primário</th>
        <th style="border: 1px double black">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->count() }}</th>
        <th style="border: 1px double black">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('vendido_cer', true)->count() }}</th>
        <th style="border: 1px double black">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('devolvido_cer', true)->count() }}</th>
        <th style="border: 1px double black">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('vendido_cer', false)->where('devolvido_cer', false)->count() }}</th>
      </tr>
    </tbody>
    <tfoot>
      <tr>
        <th style="border: 1px double black">Total Geral</th>
        <td style="border: 1px double black" class="gray">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->count() }}</td>
        <td style="border: 1px double black" class="gray">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('vendido_cer', true)->count() }}</td>
        <td style="border: 1px double black" class="gray">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('devolvido_cer', true)->count() }}</td>
        <td style="border: 1px double black" class="gray">{{ $certificados->where('distribuidor_id', '<>', 59)->where('distribuidor_id', '<>', 0)->where('vendido_cer', false)->where('devolvido_cer', false)->count() }}</td>
      </tr>
    </tfoot>
  </table>
</body>
</html>