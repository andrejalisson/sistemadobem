@extends('templates.admin')

@section('css')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title  back-change">
                <h5>{{$title}}</h5>
            </div>
            <div class="ibox-content">
                    <form role="form" method="POST" action="/Relatorio/Processamento">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="font-normal">Edição</label>
                                        <select class="select2 form-control" name="edicao">
                                            @foreach ($edicoes as $edicoes)
                                                <option value="{{$edicoes->numero_edc}}">{{$edicoes->numero_edc}}</option>  
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="font-normal">Distribuidor</label>
                                        <select class="select2 form-control" name="distribuidor">
                                            <option selected value="0">GERAL</option>
                                            @foreach ($distribuidor as $distribuidor)
                                                <option value="{{$distribuidor->id_dis}}">{{$distribuidor->nome_dis }}</option>  
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Gerar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
     $(document).ready(function(){
        $(".select2").select2({
        });
     });

</script>

@endsection