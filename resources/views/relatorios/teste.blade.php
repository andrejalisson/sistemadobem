<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Relatório</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-smfirst;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-smfirst;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>
  <table width="100%">
    <tr>
        <td valign="top"><img src="http://bemdagente.com/wp-content/uploads/2019/09/logo-150.png" width="150px" alt="100px"></td>
        <td align="right">
            <pre align="right">
              Edição:<strong> {{ $edicao }}<strong>
              Data:{{" ".date('d/m/Y')}}
              Hora:{{" ".date('H:i:s')}}
              .
          </pre>
        </td>
    </tr>
  </table>
  @php
      $pagina = 0;
  @endphp
        
      @if ($certificados->count() > 0)
          
      
        <h5>{{$distribuidores->nome_dis}}</h5>
        <table width="100%">
          <thead style="background-color: lightgray;">
            <tr>
              <th>Distribuido</th>
              <th>Vendido</th>
              <th>Devolvido</th>
              <th>Extravio</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{ $certificados->count() }}</td>
              <td>{{ $certificados->where('vendido_cer', true)->count() }}</td>
              <td>{{ $certificados->where('devolvido_cer', true)->count() }}</td>
              <td align="right">{{ $certificados->where('vendido_cer', false)->where('devolvido_cer', false)->count() }}</td>
            </tr>
          </tbody>
        </table>
      @endif
      @php
          $total = ($certificados->where('vendido_cer', true)->count()*2.5)-($certificados->where('vendido_cer', false)->where('devolvido_cer', false)->count()*10);
      @endphp
      <table width="100%">
        <tr>
            <td valign="top"></td>
            <td align="right">
                <pre align="right">
                  Valor Bruto:<strong>R$ {{ number_format($certificados->where('vendido_cer', true)->count()*10, 2) }}</strong>
                  Comissão:<strong>R$ {{ number_format($certificados->where('vendido_cer', true)->count()*2.5, 2) }}</strong>
                  Extraviados:<strong>R$ {{ number_format($certificados->where('vendido_cer', false)->where('devolvido_cer', false)->count()*10, 2) }}</strong>
                  Valor Líquido:<strong>R$ {{ number_format($total, 2) }}</strong>
                  .
                </pre>
            </td>
        </tr>
      </table>
  <br><br>
  @foreach ($extraviados as $extraviados)
  @php
      switch (strlen($extraviados->numero_cer)) {
                    case 1:
                        $complemento = "000.00".$extraviados->numero_cer;
                        break;
                    case 2:
                        $complemento = "000.0".$extraviados->numero_cer;
                        break;
                    case 3:
                        $complemento = "000.".$extraviados->numero_cer;
                        break;
                    case 4:
                        $complemento = "00".substr($extraviados->numero_cer, -4, 1).".".substr($extraviados->numero_cer, -3);
                        break;
                    case 5:
                        $complemento = "0".substr($extraviados->numero_cer, -5, 2).".".substr($extraviados->numero_cer,-3);
                        break;
                    
                    default:
                        $complemento = substr($extraviados->numero_cer, -6, 3).".".substr($extraviados->numero_cer, -3);
                        break;
                }
  @endphp
  {{ $complemento." - " }}
      
  @endforeach
</body>
</html>