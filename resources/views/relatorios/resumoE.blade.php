<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Relatório</title>

<style type="text/css">
    * {
        font-family: Verdana, Arial, sans-serif;
    }
    table{
        font-size: x-small;
    }
    tfoot tr td{
        font-weight: bold;
        font-size: x-small;
    }
    .gray {
        background-color: lightgray
    }
</style>

</head>
<body>
  <table width="100%">
    <tr>
        <td valign="top"><img src="http://bemdagente.com/wp-content/uploads/2019/09/logo-150.png" width="150px" alt="100px"></td>
        <td align="right">
            <pre align="right">
              Edição:<strong> 010<strong>
              Data:{{" ".date('d/m/Y')}}
              Hora:{{" ".date('H:i:s')}}
              .
          </pre>
        </td>
    </tr>
  </table>
  <table width="100%">
    <thead style="background-color: lightgray;">
      <tr>
        <th>Distribuidor</th>
        <th>Extravio</th>
      </tr>
    </thead>
    <tbody>
  @php
      $totalExtravio = 0;
  @endphp
  @foreach ($certificados as $certificados)
  
    
      @php
              $extravio = $certificados->extravio;

              $totalExtravio = $totalExtravio+$extravio;
      @endphp
      
        <tr>
          @foreach ($distribuidor->where('id_dis', $certificados->distribuidor_id) as $distribuidores)
            <th style="border: 1px double black">{{$distribuidores->nome_dis}}</th>
          @endforeach
          <td style="border: 1px double black" align="right">{{$extravio}}</td>
        </tr>
      
  @endforeach
    </tbody>
    <tfoot>
      <tr>
        <th style="border: 1px double black">Total Geral</th>

        <td align="right" style="border: 1px double black" class="gray">{{ $totalExtravio }}</td>
      </tr>
    </tfoot>
  </table>
</body>
</html>