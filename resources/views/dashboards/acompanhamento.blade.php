@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
<div class="row">
    @php
        $primario = 0;
        $secundario = 0;
    @endphp
    @foreach ($distribuidor as $distribuidor)
        @php
            $cearaA = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','<=', 75000)->count();
            $cearaB = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 75000)->where('sequencial','<=', 150000)->count();
            $fortalezaA = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 150000)->where('sequencial','<=', 225000)->count();
            $fortalezaB = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 225000)->count();
            
            $primario = $primario+$fortalezaA+$cearaA;
            $secundario = $secundario+$cearaB+$fortalezaB;
            
        @endphp
        @if ($cearaA+$cearaB+$fortalezaA+$fortalezaB == 0)
        @else
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ $distribuidor->NM_REVENDEDOR }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $cearaA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $cearaB }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $fortalezaA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $fortalezaB }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ $cearaA+$fortalezaA }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ $cearaB+$fortalezaB }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        @endif
        
    @endforeach
    
    
    

</div>
<div class="row">
    <div class="col-lg-6">
        <div class="widget navy-bg no-padding">
            <div class="p-m">
                @php
                    $total =($primario*5) + ($secundario*2);
                @endphp
                <h1 class="m-xs">R${{ number_format($total,2,",",".")}}</h1>

                <h3 class="font-bold no-margins">
                    Valor Bruto
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart1"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget yellow-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $primario }}</h1>

                <h3 class="font-bold no-margins">
                    Certificados Primários
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget lazur-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $secundario }}</h1>

                <h3 class="font-bold no-margins">
                    Certificados Secundário
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('script')

@endsection