@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
<div class="row">
    @php
        $cearaAA = 0;
        $cearaBB = 0;
        $fortalezaAA = 0;
        $fortalezaBB = 0;
    @endphp
    @foreach ($distribuidor as $distribuidor)
        @php
            $cearaA = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','<=', 60000)->count();
            $cearaB = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 60000)->where('sequencial','<=', 120000)->count();
            $fortalezaA = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 120000)->where('sequencial','<=', 180000)->count();
            $fortalezaB = $certificados->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 180000)->count();
            
            $cearaAA = $cearaAA+$cearaA;
            $cearaBB = $cearaBB+$cearaB;
            $fortalezaAA = $fortalezaAA+$fortalezaA;
            $fortalezaBB = $fortalezaBB+$fortalezaB;
            
        @endphp
        @if ($cearaA+$cearaB+$fortalezaA+$fortalezaB == 0)
        @else
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ $distribuidor->NM_REVENDEDOR }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $cearaA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $cearaB }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $fortalezaA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $fortalezaB }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ $cearaA+$fortalezaA }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ $cearaB+$fortalezaB }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <h2><span class="label label-primary"><Strong>{{ $cearaA+$fortalezaA+$cearaB+$fortalezaB }}</Strong></span></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        @endif
        
    @endforeach
    
    
    

</div>
<div class="row">
    <div class="col-lg-3">
        <div class="widget yellow-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $cearaAA }}</h1>

                <h3 class="font-bold no-margins">
                    Ceará A
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget lazur-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $cearaBB }}</h1>

                <h3 class="font-bold no-margins">
                    Ceará B
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget yellow-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $fortalezaAA }}</h1>

                <h3 class="font-bold no-margins">
                    Fortaleza A
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget lazur-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $fortalezaBB }}</h1>

                <h3 class="font-bold no-margins">
                    Fortaleza B
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
@endsection

@section('script')

@endsection