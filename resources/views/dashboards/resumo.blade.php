@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
<div class="row">
    @php
        $cearaAA = 0;
        $cearaBB = 0;
        $fortalezaAA = 0;
        $fortalezaBB = 0;
    @endphp
    @foreach ($distribuidor as $distribuidor)
        @php
            $cearaAVendido = $certificadosVendidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','<=', 60000)->count();
            $cearaBVendido = $certificadosVendidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 60000)->where('sequencial','<=', 120000)->count();
            $fortalezaAVendido = $certificadosVendidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 120000)->where('sequencial','<=', 180000)->count();
            $fortalezaBVendido = $certificadosVendidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 180000)->count();

            $cearaADevolvido = $certificadosDevolvidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','<=', 60000)->count();
            $cearaBDevolvido = $certificadosDevolvidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 60000)->where('sequencial','<=', 120000)->count();
            $fortalezaADevolvido = $certificadosDevolvidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 120000)->where('sequencial','<=', 180000)->count();
            $fortalezaBDevolvido = $certificadosDevolvidos->where('cd_revendedor', $distribuidor->CD_REVENDEDOR)->where('sequencial','>', 180000)->count();


            $cearaADistribuido = $certificadosDistribuidos->where('ID_REVENDEDOR', $distribuidor->CD_REVENDEDOR)->where('FINAL','<=', 60000);
            $cearaBDistribuido = $certificadosDistribuidos->where('ID_REVENDEDOR', $distribuidor->CD_REVENDEDOR)->where('FINAL','>', 60000)->where('sequencial','<=', 120000);
            $fortalezaADistribuido = $certificadosDistribuidos->where('ID_REVENDEDOR', $distribuidor->CD_REVENDEDOR)->where('FINAL','>', 120000)->where('sequencial','<=', 180000);
            $fortalezaBDistribuido = $certificadosDistribuidos->where('ID_REVENDEDOR', $distribuidor->CD_REVENDEDOR)->where('FINAL','>', 180000);


            $TotalCA = 0;
            $TotalCB = 0;
            $TotalFA = 0;
            $TotalFB = 0;
            foreach ($cearaADistribuido as $cearaADistribuido) {
                $TotalCA = $TotalCA + $cearaADistribuido->FINAL - $cearaADistribuido->INICIAL+1;
            }
            foreach ($cearaBDistribuido as $cearaBDistribuido) {
                $TotalCB = $TotalCB + $cearaBDistribuido->FINAL - $cearaBDistribuido->INICIAL+1;
            }
            foreach ($fortalezaADistribuido as $fortalezaADistribuido) {
                $TotalFA = $TotalFA + $fortalezaADistribuido->FINAL - $fortalezaADistribuido->INICIAL+1;
            }
            foreach ($fortalezaBDistribuido as $fortalezaBDistribuido) {
                $TotalFB = $TotalFB + $fortalezaBDistribuido->FINAL - $fortalezaBDistribuido->INICIAL+1;
            }
            
        @endphp
        @if ($cearaA+$cearaB+$fortalezaA+$fortalezaB == 0)
        @else
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Distribuído</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $TotalCA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $TotalCB }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $TotalFA }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $TotalFB }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ $TotalCA+$TotalFA }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ $TotalCB+$TotalFB }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Vendido</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $cearaAVendido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $cearaBVendido }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $fortalezaAVendido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $fortalezaBVendido }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ $cearaAVendido+$fortalezaAVendido }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ $cearaBVendido+$fortalezaBVendido }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Devolvidos</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $cearaADevolvido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $cearaBDevolvido }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $fortalezaADevolvido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $fortalezaBDevolvido }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ $cearaADevolvido+$fortalezaADevolvido }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ $cearaBDevolvido+$fortalezaBDevolvido }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Extravio</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-3">
                                <small class="stats-label">Ceará R$5,00</small>
                                <strong><h4>{{ $TotalCA-$cearaADevolvido-$cearaAVendido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Ceará R$2,00</small>
                                <strong><h4>{{ $TotalCB-$cearaBDevolvido-$cearaBVendido }}</h4></strong>
                            </div>

                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$5,00</small>
                                <strong><h4>{{ $TotalFA-$fortalezaADevolvido-$fortalezaAVendido }}</h4></strong>
                            </div>
                            <div class="col-3">
                                <small class="stats-label">Fortaleza R$2,00</small>
                                <strong><h4>{{ $TotalFB-$fortalezaBDevolvido-$fortalezaBVendido }}</h4></strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Certificado Primário</span>
                                <strong><h4>{{ ($TotalCA-$cearaADevolvido-$cearaAVendido)+($TotalFA-$fortalezaADevolvido-$fortalezaAVendido) }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Certificado Secundário</span>
                                <strong><h4>{{ ($TotalCB-$cearaBDevolvido-$cearaBVendido)+($TotalFB-$fortalezaBDevolvido-$fortalezaBVendido) }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
    @endforeach
    
    
    

</div>
@endsection

@section('js')
@endsection

@section('script')

@endsection