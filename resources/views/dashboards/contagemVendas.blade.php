@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
<div class="row">
    @php
        $primario = 0;
        $secundario = 0;

        

    @endphp
    @foreach ($distribuidor as $distribuidor)
        @php

            $distribuidosPrimarios      = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '<=', 90000)->count();
            $vendidosPrimarios          = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '<=', 90000)->where('vendido_cer', true)->count();
            $devolvidosPrimarios        = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '<=', 90000)->where('devolvido_cer', true)->count();
            $extraviosPrimarios         = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '<=', 90000)->where('vendido_cer', false)->where('devolvido_cer', false)->count();

            $distribuidosSecundarios    = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '>', 90000)->count();
            $vendidosSecundarios        = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '>', 90000)->where('vendido_cer', true)->count();
            $devolvidosSecundarios      = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '>', 90000)->where('devolvido_cer', true)->count();
            $extraviosSecundarios       = $certificados->where('distribuidor_id', $distribuidor->id_dis)->where('numero_cer', '>', 90000)->where('vendido_cer', false)->where('devolvido_cer', false)->count();
            
            $primario = $primario+$vendidosPrimarios;
            $secundario = $secundario+$vendidosSecundarios;
        @endphp
        @if ($distribuidosPrimarios+$distribuidosSecundarios == 0)
        @else
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>{{ $distribuidor->nome_dis }}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Distribuidos Primário</span>
                                <strong><h4>{{ $distribuidosPrimarios }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Distribuidos Secundário</span>
                                <strong><h4>{{ $distribuidosSecundarios }}</h4></strong>
                            </div>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Vendidos Primário</span>
                                <strong><h4>{{ $vendidosPrimarios }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Vendidos Secundário</span>
                                <strong><h4>{{ $vendidosSecundarios }}</h4></strong>
                            </div>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Devolvidos Primário</span>
                                <strong><h4>{{ $devolvidosPrimarios }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Devolvidos Secundário</span>
                                <strong><h4>{{ $devolvidosSecundarios }}</h4></strong>
                            </div>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-6">
                                <span class="label label-primary">Extravio Primário</span>
                                <strong><h4>{{ $extraviosPrimarios }}</h4></strong>
                            </div>
                            <div class="col-6">
                                <span class="label label-warning">Extravio Secundário</span>
                                <strong><h4>{{ $extraviosSecundarios }}</h4></strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        @endif
        
    @endforeach
    

</div>
<div class="row">
    <div class="col-lg-3">
        <div class="widget lazur-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $primario }}</h1>

                <h3 class="font-bold no-margins">
                    Vendidos Primário
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="widget lazur-bg no-padding">
            <div class="p-m">
                <h1 class="m-xs">{{ $secundario }}</h1>

                <h3 class="font-bold no-margins">
                    Vendidos Secundário
                </h3>
            </div>
            <div class="flot-chart">
                <div class="flot-chart-content" id="flot-chart2"></div>
            </div>
        </div>
    </div>
</div>

    

@endsection

@section('js')
@endsection

@section('script')

@endsection