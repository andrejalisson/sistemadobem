@extends('templates.admin')

@section('css')
<link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <div class="ibox-tools">
                    <button class="btn btn-primary" onclick="location.href='/Distribuidores/Adicionar'"  type="button"><i class="fa fa-plus"></i> Adicionar</button>
                </div>
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="distribuidor" class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th style='width: 5%;'>#</th>
                        <th>Nome</th>
                        <th>Responsável</th>
                        <th>Master</th>
                        <th>Região</th>
                        <th>Telefone</th>
                        <th style='width: 15%;'>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
@endsection

@section('script')
<script>
    $(function () {
        $(document).ready(function () {
            $('#distribuidor').dataTable({
                pageLength: 10,
                responsive: true,
                processing: true,
                serverSide: true,
                oLanguage: {
                "sLengthMenu": "Mostrar _MENU_ registros por página",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
                "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros)",
                "sSearch": "Pesquisar: ",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                    }
                },
                ajax:{
                     "url": "{{ url('todosDistribuidores') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{
                            _token: "{{csrf_token()}}"                     
                     }
                   },
                columns: [
                    { "data": "id" },
                    { "data": "nome" },
                    { "data": "responsavel" },
                    { "data": "master" },
                    { "data": "regiao" },
                    { "data": "telefone" },
                    { "data": "opcoes" }
            ]
            });

            
        });
    });

</script>

@endsection