@extends('templates.admin')

@section('css')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title  back-change">
                <h5>{{$title}}</h5>
            </div>
            <div class="ibox-content">
                    <form role="form" method="POST" action="/Distribuidores/Editar/{{$distribuidor->id_dis}}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Distribuição</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="nome" value="{{$distribuidor->nome_dis}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Responsável</label>
                                        <div class="input-group">
                                        <input type="text" class="form-control" name="responsavel" value="{{$distribuidor->responsavel_dis}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Telefone</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" data-mask="(99) 9 9999-9999" name="telefone" value="{{$distribuidor->telefone_dis}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="font-normal">Distribuidor Master</label>
                                        <select class="select2 form-control" name="master">
                                            @foreach ($master as $master)
                                            @if ($distribuidor->regiaoId_dis == $master->id_mas)
                                            <option selected value="{{$master->id_mas}}">{{$master->nome_mas}}</option>
                                            @else
                                            <option value="{{$master->id_mas}}">{{$master->nome_mas}}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="font-normal">Região</label>
                                        <select class="select2 form-control" name="regiao">
                                            @foreach ($regiao as $regiao)
                                            @if($distribuidor->regiaoId_dis == $regiao->id_reg)
                                                <option selected value="{{$regiao->id_reg}}">{{$regiao->nome_reg}}</option>
                                            @else
                                                <option value="{{$regiao->id_reg}}">{{$regiao->nome_reg}}</option>
                                            @endif
                                                  
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Editar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
     $(document).ready(function(){
        $(".select2").select2({
        });
     });

</script>

@endsection