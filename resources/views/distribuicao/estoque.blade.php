@extends('templates.admin')

@section('css')
<link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" id="distribuicaoForm">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <fieldset>
                                <div class="form-group">
                                    <label class="font-normal">Edição</label>
                                    <select class="select2 form-control" id="edicao" name="edicao">
                                        @foreach ($edicao as $edicao)
                                        @php
                                            $sorteio = str_replace('-', '/', $edicao->sorteio_edc);
                                            $sorteio = date('d/m/Y', strtotime($sorteio));
                                        @endphp
                                            <option value="{{$edicao->numero_edc}}">{{$edicao->numero_edc." - ".$sorteio}}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-4">
                            <fieldset>
                                <div class="form-group">
                                    <label class="font-normal">Distribuidor</label>
                                    <select class="select2 form-control" autofocus id="distribuidor" name="distribuidor">
                                        @foreach ($distribuidor as $distribuidor)
                                            <option value="55">Estoque</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-4">
                            <label>Certificado Inicial</label>
                            <input type="text" id="inicial" required name="inicial" class="form-control" placeholder="Inicial">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="/js/plugins/select2/select2.full.min.js"></script>
<script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $(".select2").select2();
        $('.select2').select2('focus');
        $('.select2').select2('open');
    });    
    $("#distribuicaoForm").submit(function(event){
        event.preventDefault();
        iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: " Informação sendo processada...",
        });
        var distribuidorx = $('#distribuidor').val();
        var inicialx = $('#inicial').val();
        var finalx = $('#final').val();
        var edicao = $("#edicao").val();
        $('#quantidade').val("");
        $('#inicial').val("");
        $('#final').val("");
        $('#inicial').focus();
        $.ajax({
            url: '/Distribuicao/Estoque',
            type: 'POST', 
            data: {
                _token: "{{csrf_token()}}",
                distribuidor: distribuidorx,
                lote: lotex,
                edc: edicao
                },
            success: function(resposta){
                iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: resposta+" Certificados Distribuídos com Sucesso",
                });
            },
            error: function(resposta){
                iziToast.error({
                title: 'O.o',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "ERRO: Favor Repetir o Lote",
            });
            }
        })
    });
    	
</script>

@endsection