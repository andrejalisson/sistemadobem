@extends('templates.admin')

@section('css')
<link href="/css/plugins/dataTables/datatables.min.css" rel="stylesheet">
<link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" id="distribuicaoForm">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-sm-3">
                            <fieldset>
                                <div class="form-group">
                                    <label class="font-normal">Edição</label>
                                    <select class="select2 form-control" id="edicao" name="edicao">
                                        @foreach ($edicao as $edicao)
                                        @php
                                            $sorteio = str_replace('-', '/', $edicao->sorteio_edc);
                                            $sorteio = date('d/m/Y', strtotime($sorteio));
                                        @endphp
                                            <option value="{{$edicao->numero_edc}}">{{$edicao->numero_edc." - ".$sorteio}}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-3">
                            <fieldset>
                                <div class="form-group">
                                    <label class="font-normal">Distribuidor</label>
                                    <select class="select2 form-control" autofocus id="distribuidor" name="distribuidor">
                                        @foreach ($distribuidor as $distribuidor)
                                            <option value="{{$distribuidor->id_dis}}">{{$distribuidor->nome_dis}}</option>  
                                        @endforeach
                                    </select>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-sm-3">
                            <label>Certificado Inicial</label>
                            <input type="text" id="inicial" required data-mask="999.999" name="inicial" class="form-control" placeholder="Inicial">
                        </div>
                        <div class="col-sm-3">
                            <label>Certificado Final</label>
                            <input type="text" id="final" data-mask="999.999" required name="final" class="form-control" placeholder="Final">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-content">
                <div class="table-responsive">
                    <table id="distribuicaoDistribuidor" class="table table-striped table-bordered table-hover" >
                    <thead>
                    <tr>
                        <th style='width: 5%;'>#</th>
                        <th>Edição</th>
                        <th>Distribuidor</th>
                        <th>Inicial</th>
                        <th>Final</th>
                        <th>Total</th>
                        <th style='width: 15%;'>Opções</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
    <script src="/js/plugins/dataTables/datatables.min.js"></script>
    <script src="/js/plugins/dataTables/dataTables.bootstrap4.min.js"></script>
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $(".select2").select2();
        $('.select2').select2('focus');
        $('.select2').select2('open');
    });    
    $("#distribuicaoForm").submit(function(event){
        event.preventDefault();
        iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: " Informação sendo processada...",
        });
        var distribuidorx = $('#distribuidor').val();
        var inicialx = $('#inicial').val();
        var finalx = $('#final').val();
        var edicao = $("#edicao").val();
        $('#quantidade').val("");
        $('#inicial').val("");
        $('#final').val("");
        $('#inicial').focus();
        
        $.ajax({
            url: '/Distribuicao/AdicionarDistribuicao',
            type: 'POST', 
            data: {
                _token: "{{csrf_token()}}",
                distribuidor: distribuidorx,
                inicial: inicialx,
                final: finalx,
                edc: edicao
                },
            success: function(resposta){
                iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: resposta+" Certificados Distribuídos com Sucesso",
                });
                $('#distribuicaoDistribuidor').DataTable().ajax.reload();
            },
            error: function(resposta){
                iziToast.error({
                    title: 'O.o',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "ERRO: Favor Repetir o Lote",
                });
            }
        })
        
        
    });
    $(function () {
        $(document).ready(function () {
            $('#distribuicaoDistribuidor').dataTable({
                pageLength: 10,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [[ 0, "desc" ]],
                oLanguage: {
                "sLengthMenu": "Mostrar _MENU_ registros por página",
                "sZeroRecords": "Nenhum registro encontrado",
                "sInfo": "Mostrando _END_ de _TOTAL_ registro(s)",
                "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                "sInfoFiltered": "(filtrado de _MAX_ registros)",
                "sSearch": "Pesquisar: ",
                "oPaginate": {
                    "sFirst": "Início",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                    }
                },
                ajax:{
                     "url": "{{ url('todasDistribuicoesDistribuidores') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{
                            _token: "{{csrf_token()}}"                     
                     }
                   },
                columns: [
                    { "data": "id" },
                    { "data": "edicao" },
                    { "data": "distribuidor" },
                    { "data": "inicial" },
                    { "data": "final" },
                    { "data": "total" },
                    { "data": "opcoes" }
            ]
            });

            
        });
    });

</script>

@endsection