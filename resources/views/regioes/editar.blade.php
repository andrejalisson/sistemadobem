@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" action="/EditarRegiao/{{ $regiao->id_reg }}">
                    {!! csrf_field() !!}
                    <div class="form-group row"><label class="col-sm-2 col-form-label">Nome</label>
                        <div class="col-sm-6">
                            <input type="text" required value="{{ $regiao->nome_reg }}" name="nome" class="form-control" placeholder="Nome da região">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" type="submit">Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    

@endsection

@section('script')


@endsection