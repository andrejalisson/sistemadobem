@extends('templates.admin')

@section('css')
@endsection

@section('corpo')
@if(isset($certificado))
    @foreach($certificado as $certificado)
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox product-detail">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="product-images">
                                    <div>
                                        <div>
                                        <img src="data:image/jpeg;base64, {{ $certificado->imagem }}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <h2 class="font-bold m-b-xs">
                                    {{$certificado->NM_REVENDEDOR}}
                                </h2>
                                <div class="m-t-md">
                                    <h2 class="product-main-price">#{{$certificado->sequencial}} <small class="text-muted">Certificado 01</small> #{{$certificado->sequencial + 300000}} <small class="text-muted">Certificado 02</small> </h2>
                                </div>
                                <hr>
                                <h4>Lote: {{$certificado->lote}}</h4>
                                <h4>Sequência: {{$certificado->ordem}}</h4>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-footer">
                        <span class="float-right">
                            Pesquisa gerada - <i class="fa fa-clock-o"></i> {{date('d/m/Y H:i')}}
                        </span>
                        Pesquisa para simples conferência.
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
@endsection

@section('js')
@endsection

@section('script')
@endsection

