<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{$title}}</title>

    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/0fbdb8a91f.js" crossorigin="anonymous"></script>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    
    <link href="/css/plugins/slick/slick.css" rel="stylesheet">
    <link href="/css/plugins/slick/slick-theme.css" rel="stylesheet">
    <link href="/js/iziToast/dist/css/iziToast.min.css" rel="stylesheet" >
    @yield('css')
    <link href="/css/animate.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">



</head>

<body>

<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="block m-t-xs font-bold">{{session('login')}}</span>
                        </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a class="dropdown-item" href="profile.html">Perfil</a></li>
                            <li class="dropdown-divider"></li>
                            <li><a class="dropdown-item" href="login.html">Sair</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        BDG
                    </div>
                </li>
                {{-- <li>
                    <a href="#"><i class="fas fa-globe"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Dashboards/Vendas">Contagem das Vendas</a></li>
                        <li><a href="/Dashboards/Financeiro">Financeiro</a></li>
                    </ul>
                </li> --}}
                <li>
                    <a href="#"><i class="far fa-file"></i></i> <span class="nav-label">Relatórios</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Relatorio/Processamento">Relatório do Processamento</a></li>
                        {{-- <li><a href="/Relatorio/ExtravioResumo">Relatório Resumo Extravio</a></li> --}}
                    </ul>
                </li>
                
                <li>
                    <a href="#"><i class="fas fa-map-marked-alt"></i> <span class="nav-label">Regiões</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Regioes/Listar">Listar</a></li>
                        <li><a href="/Regioes/AdicionarRegioes">Adicionar</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fas fa-city"></i> <span class="nav-label">Distribuidor Master</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Master/Listar">Listar</a></li>
                        <li><a href="/Master/Adicionar">Adicionar</a></li>
                    </ul>
                </li>

                <li>
                    <a href="#"><i class="fas fa-city"></i> <span class="nav-label">Distribuidores</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Distribuidores/Listar">Listar</a></li>
                        <li><a href="/Distribuidores/Adicionar">Adicionar</a></li>
                        <li><a href="/Distribuidores/Migrar">Migrar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-address-card"></i> <span class="nav-label">Vendedores</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Vendedores/Listar">Listar</a></li>
                        <li><a href="/Vendedores/Adicionar">Adicionar</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-gift"></i> <span class="nav-label">Premiação</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="#">Lista de Ganhadores</a></li>
                        <li><a href="#">Termos</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-cubes"></i> <span class="nav-label">Distribuição</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Distribuicao/listar">Distribuidor</a></li>
                        <li><a href="#">Vendedor</a></li>
                        <li><a href="/Distribuicao/Estoque">Estoque</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-hand-holding-usd"></i> <span class="nav-label">Financeiro</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Financeiro/Contas">Contagem de Instantâneas</a></li>
                        {{-- <li><a href="#">Fluxo de Caixa</a></li>
                        <li><a href="#">Contas a Pagar</a></li>
                        <li><a href="#">Contas a Receber</a></li>
                        <li><a href="#">Orçamentos</a></li>
                        <li><a href="/Financeiro/Contas">Contas</a></li> --}}
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fas fa-bahai"></i> <span class="nav-label">Edição</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Edicao/listar">Listar</a></li>
                        <li><a href="/Edicao/AdicionarEdicao">Adicionar</a></li>
                        <li><a href="/Edicao/Certificados">Certificados</a></li>
                        
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fas fa-microchip"></i> <span class="nav-label">CPD</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Processamento/DevolucaoIGS">Devolução</a></li>
                        <li><a href="/Processamento/ContagemVenda">Venda</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fas fa-microchip"></i> <span class="nav-label">Matriz</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Matriz/Listar">Listar</a></li>
                        <li><a href="/Matriz/Gerar">Gerar Matriz</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Usuários</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="/Usuarios/Listar">Listar</a></li>
                        <li><a href="/Usuarios/Adicionar">Adicionar</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                    <form role="search" class="navbar-form-custom" method="post" action="/Pesquisa">
                    {!! csrf_field() !!}
                        <div class="form-group">
                            <input type="text" placeholder="Buscar Certificado" class="form-control" name="pesquisa" id="top-search">
                        </div>
                    </form>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                        </a>
                        <ul class="dropdown-menu dropdown-alerts">
                            <li>
                                <a href="mailbox.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                        <span class="float-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a href="profile.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="float-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <a href="grid_options.html" class="dropdown-item">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="float-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="dropdown-divider"></li>
                            <li>
                                <div class="text-center link-block">
                                    <a href="notifications.html" class="dropdown-item">
                                        <strong>See All Alerts</strong>
                                        <i class="fa fa-angle-right"></i>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>


                    <li>
                        <a href="login.html">
                            <i class="fa fa-sign-out"></i> Sair
                        </a>
                    </li>
                </ul>

            </nav>
        </div>
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>{{$title}}</h2>
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            @yield('corpo')
        </div>
        <div class="footer">
            <div class="float-right">
                André Jálisson<strong><a target="_black" href="https://wa.me/558586084743">85 9 8608-4743.</a></strong>
            </div>
            <div>
                <strong>&copy; Todos os direitos reservados</strong> Bem da gente  {{date('Y')}}
            </div>
        </div>

    </div>
</div>



<!-- Mainly scripts -->
<script src="/js/jquery-3.1.1.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<!-- Custom and plugin javascript -->
<script src="/js/inspinia.js"></script>
<script src="/js/plugins/pace/pace.min.js"></script>
<script src="/js/iziToast/dist/js/iziToast.min.js"></script>
<script src="/js/jquery.loading.min.js"></script>

@yield('js')
<!-- slick carousel-->
<script src="/js/plugins/slick/slick.min.js"></script>
@yield('script')

    
<script>
    $(document).ready(function(){
        @if (session('sucesso'))
            iziToast.success({
                title: ':)',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "{{session('sucesso')}}",
            });
        @endif
        @if (session('alerta'))
            iziToast.error({
                title: 'O.o',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "{{session('alerta')}}",
            });
        @endif

    });

</script>

</body>

</html>
