@extends('templates.admin')

@section('css')
<link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" id="devolucaoForm" action="">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="font-normal">Código Inicial</label>
                            <input type="text" name="nome" id="codigoInicial" autocomplete="off" autofocus class="form-control" placeholder="Código Inicial">
                        </div>
                        <div class="col-sm-4">
                            <label class="font-normal">Código Final</label>
                            <input type="text" name="nome" id="codigoFinal" autocomplete="off" class="form-control" placeholder="Código Final">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                   
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="/js/plugins/select2/select2.full.min.js"></script>
<script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
    var codigoX = new Array();
    $('#codigoInicial').keypress(function(e) {
        if(e.which == 13){
            e.preventDefault();
            $('#codigoFinal').focus();
        }
    });
    $('#codigoFinal').keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
            var string1 = $("#codigoInicial").val();
            var string2 = $("#codigoFinal").val();
            var inicial1 = string1.substr(5,(string1.length - 0));
            var inicial = inicial1.substr(0,(inicial1.length - 1));
            var final1 = string2.substr(5,(string2.length - 0));
            var final = final1.substr(0,(final1.length - 1));
            var edicao = string1.substr(0,(string1.length - 9));

            $('body').loading({
                stoppable: true,
                message: 'Salvando...'
            });
            $.ajax({
                url: '/Processamento/DevolucaoBIP',
                type: 'POST', 
                data: {
                    _token: "{{csrf_token()}}",
                    edc: edicao,
                    inicial1: inicial,
                    final1: final
                    },
                success: function(resposta){
                    iziToast.success({
                        title: ':)',
                        transitionIn: 'bounceInLeft',
                        position: 'topRight',
                        message: resposta+" Certificados Devolvidos com Sucesso",
                    });
                
                    $('#codigoInicial').val("");
                    $('#codigoFinal').val("");
                    $('#codigoInicial').focus();
                    $('body').loading('stop');
                },
                error: function(resposta){
                    iziToast.error({
                    title: 'O.o',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "ERRO: Favor Repetir o Lote",
                });
                $('#codigoInicial').val("");
                $('#codigoFinal').val("");
                $('#codigoInicial').focus();
                $('body').loading('stop');
                }
            })
            $('#codigoInicial').val("");
            $('#codigoFinal').val("");
            $('#codigoInicial').focus();
        }
    });
   
    </script>

@endsection