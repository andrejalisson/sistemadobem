@extends('templates.admin')

@section('css')
<link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
<link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>{{ $title }}</h5>
            </div>
            <div class="ibox-content">
                <form method="post" id="devolucaoForm" action="">
                    {!! csrf_field() !!}
                    <div class="form-group row">
                        <div class="col-sm-4">
                            <label class="font-normal">Certificado</label>
                            <input type="text" name="nome" id="codigo" autocomplete="off" autofocus class="form-control" placeholder="Código do certificado">
                        </div>
                        <div class="col-sm-4">
                            <label class="font-normal">Código Inicial</label>
                            <input type="text" name="nome" id="codigoInicial" autocomplete="off" autofocus class="form-control" placeholder="Código Inicial">
                        </div>
                        <div class="col-sm-4">
                            <label class="font-normal">Código Final</label>
                            <input type="text" name="nome" id="codigoFinal" autocomplete="off" class="form-control" placeholder="Código Final">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <div class="widget navy-bg p-lg text-center">
                                <div class="m-b-md">
                                    <i class="fas fa-barcode fa-4x"></i>
                                    <h1 class="m-xs" id="igs">0</h1>
                                    <h3 class="font-bold no-margins">
                                        BILHETES(<small>Certificados</small>) IGS
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="widget yellow-bg p-lg text-center">
                                <div class="m-b-md">
                                    <i class="fas fa-barcode fa-4x"></i>
                                    <h1 class="m-xs" id="bip">0</h1>
                                    <h3 class="font-bold no-margins">
                                        BILHETES(<small>Certificados</small>) BIP
                                    </h3>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script src="/js/plugins/select2/select2.full.min.js"></script>
<script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
    $(document).ready(function(){
        $(".select2").select2({
        });
    });
    $('#codigoInicial').keypress(function(e) {
        if(e.which == 13){
            e.preventDefault();
            $('#codigoFinal').focus();
        }
    });
    var codigoIGS = new Array();
    var edicaoIGS = "0";
    var inicial = "0";
    var final = "0";
    $('#codigo').keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
            var string = $("#codigo").val();
            var certificado1 = string.substr(3,(string.length - 1));
            var certificado = certificado1.substr(0,(certificado1.length - 1));
            edicaoIGS = string.substr(0,(string.length - 7));
            if(codigoIGS.indexOf(certificado) == -1){
                codigoIGS.push( certificado);
                var IGS = $("#igs").html();
                IGS = parseInt(IGS) + 1;
                $('#igs').html(IGS);
            } 
            $('#codigo').val("");
            $('#codigo').focus(); 
        }
    });
    $('#codigoFinal').keypress(function(e) {
        if(e.which == 13) {
            e.preventDefault();
            var string1 = $("#codigoInicial").val();
            var string2 = $("#codigoFinal").val();
            var inicial1 = string1.substr(5,(string1.length - 0));
            inicial = inicial1.substr(0,(inicial1.length - 1));
            var final1 = string2.substr(5,(string2.length - 0));
            final = final1.substr(0,(final1.length - 1));
            edicaoIGS = string1.substr(0,(string1.length - 9));
            while (inicial<=final) {
                if(codigoIGS.indexOf(parseInt(inicial)) == -1){
                    codigoIGS.push(parseInt(inicial));
                    var BIP = $("#bip").html();
                    BIP = parseInt(BIP) + 1;
                    $('#bip').html(BIP);
                }
                inicial++;
            }
            $('#codigoInicial').val("");
            $('#codigoFinal').val("");
            $('#codigoInicial').focus();
            
        }
    });
    $("#devolucaoForm").submit(function(event){
        event.preventDefault();
        $('body').loading({
            stoppable: true,
            message: 'Salvando...'
        });
        $.ajax({
            url: '/Processamento/Devolucao',
            type: 'POST', 
            data: {
                _token: "{{csrf_token()}}",
                certificadosIGS: codigoIGS,
                edcIGS: edicaoIGS
                },
            success: function(resposta){
                iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: resposta+" Certificados Devolvidos com Sucesso",
                });
                codigoIGS = new Array();
                $('#igs').html("0");
                $('#bip').html("0");
                $('#codigo').val("");
                $('#codigo').focus();
                $('#codigoInicial').val("");
                $('#codigoFinal').val("");
                $('body').loading('stop');
            },
            error: function(resposta){
                iziToast.error({
                title: 'O.o',
                transitionIn: 'bounceInLeft',
                position: 'topRight',
                message: "ERRO: Favor Repetir o Lote",
            });
            $('body').loading('stop');
            }
        })
    });
    </script>

@endsection