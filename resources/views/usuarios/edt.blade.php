@extends('templates.admin')

@section('css')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title  back-change">
                <h5>{{$title}}</h5>
                
            </div>
            <div class="ibox-content">
                @foreach ($usuario as $usuario)
                    <form role="form" method="POST" action="/Usuarios/Editar">
                        {!! csrf_field() !!}
                        <input type="hidden" name="id_user" value="{{ $usuario->nome_user }}">
                            <div class="row">
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">Nome</label>
                                            <div class="input-group">
                                            <input type="text" class="form-control" autocomplete="off" equired name="nome" value="{{$usuario->nome_user}}" placeholder="Nome do Usuário">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">Email</label>
                                            <div class="input-group">
                                                <input type="email" class="form-control" autocomplete="off" name="email" value="{{$usuario->email_user}}" placeholder="Email">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">CPF</label>
                                            <div class="input-group">
                                                <input type="text" data-mask="999.999.999-99" autocomplete="off" required value="{{$usuario->CPF_user}}" class="form-control" name="cpf" placeholder="CPF">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">Usuário</label>
                                            <div class="input-group">
                                                <input type="text"  class="form-control" autocomplete="off" required name="usuario" value="{{$usuario->usarname_user}}" placeholder="Usuário para acesso ao sistema.">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">Senha</label>
                                            <div class="input-group">
                                                <input type="password" id="senha" autocomplete="off" required class="form-control" name="senha1">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div class="col-md-4">
                                    <fieldset>
                                        <div class="form-group" id="data_1">
                                            <label class="font-normal">Repetir a Senha</label>
                                            <div class="input-group">
                                                <input type="password" id="senha" autocomplete="off" required class="form-control" name="senha2">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                                <div  class="col-md-12">
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group row">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    @endforeach
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')


@endsection