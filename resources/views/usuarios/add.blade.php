@extends('templates.admin')

@section('css')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title  back-change">
                <h5>{{$title}}</h5>
                
            </div>
            <div class="ibox-content">
                    <form role="form" method="POST" action="/Usuarios/Adicionar">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Nome</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" required name="nome" placeholder="Nome do Usuário">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Email</label>
                                        <div class="input-group">
                                            <input type="email" class="form-control" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">CPF</label>
                                        <div class="input-group">
                                            <input type="text" data-mask="999.999.999-99" required class="form-control" name="cpf" placeholder="CPF">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Usuário</label>
                                        <div class="input-group">
                                            <input type="text"  class="form-control" required name="usuario" placeholder="Usuário para acesso ao sistema.">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Senha</label>
                                        <div class="input-group">
                                            <input type="password" id="senha" required class="form-control" name="senha1" value="123mudar">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Repetir a Senha</label>
                                        <div class="input-group">
                                            <input type="password" id="senha" required class="form-control" name="senha2" value="123mudar">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')


@endsection