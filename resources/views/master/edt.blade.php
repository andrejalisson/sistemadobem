@extends('templates.admin')

@section('css')
    <link href="/css/plugins/select2/select2.min.css" rel="stylesheet">
    <link href="/css/plugins/select2/select2-bootstrap4.min.css" rel="stylesheet">
@endsection

@section('corpo')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title  back-change">
                <h5>{{$title}}</h5>
            </div>
            <div class="ibox-content">
                @foreach ($master as $master)
                    <form role="form" method="POST" action="/Master/Editar/{{$master->id_mas}}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Master</label>
                                        <div class="input-group">
                                        <input type="text" class="form-control" name="nome" value="{{$master->nome_mas}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Responsável</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" name="responsavel" value="{{$master->responsavel_mas}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-3">
                                <fieldset>
                                    <div class="form-group" id="data_1">
                                        <label class="font-normal">Telefone</label>
                                        <div class="input-group">
                                            <input type="text" data-mask="(99) 9 9999-9999" class="form-control" name="telefone" value="{{$master->telefone_mas}}">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="font-normal">Região</label>
                                        <select class="select2 form-control" name="regiao">
                                            @foreach ($regiao as $regiao)
                                                @if ($regiao->id_reg == $master->regiaoId_mas)
                                                    <option selected value="{{$regiao->id_reg}}">{{$regiao->nome_reg}}</option>
                                                @else
                                                    <option value="{{$regiao->id_reg}}">{{$regiao->nome_reg}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </fieldset>
                            </div>
                            <div  class="col-md-12">
                                <div class="hr-line-dashed"></div>
                                <div class="form-group row">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <button class="btn btn-primary btn-sm" type="submit">Adicionar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script src="/js/plugins/select2/select2.full.min.js"></script>
    <script src="/js/plugins/jasny/jasny-bootstrap.min.js"></script>
@endsection

@section('script')
<script>
     $(document).ready(function(){
        $(".select2").select2({
        });
     });

</script>

@endsection