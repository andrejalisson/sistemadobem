<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ $title }} | Bem da Gente</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="/js/iziToast/dist/css/iziToast.min.css" rel="stylesheet" >

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated rotateIn">
        <div>
            <div>

                <h1 class="logo-name">BDG</h1>

            </div>
            <h3>Vem pro Bem.</h3>
            <p>🤝💚🍀Ao adquirir um certificado de contribuição do Bem da Gente você transforma a vida de muitas crianças da Associação Peter Pan e ainda concorre a muitos prêmios. 🍀💚🤝<br>
                <small>#vemprobem #vemprobemdagente #associacaopeterpan #FaçaoBem</small>
            </p>
            <p>Coloque suas credenciais para continuar.</p>
            <form class="m-t" role="form" method="post" action="/Login">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" autofocus autocomplete="off" class="form-control" name="usuario" placeholder="Usuário" required="">
                </div>
                <div class="form-group">
                    <input type="password" autocomplete="off" class="form-control" name="senha" placeholder="Senha" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Entrar</button>

                <a href="/RedefinirSenha"><small>Esqueceu a senha?</small></a>
            </form>
        <p class="m-t"> <small>&copy; Todos os direitos reservados Bem da gente {{date('Y')}}</small> </p>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="/js/iziToast/dist/js/iziToast.min.js"></script>
    <script>
        $(document).ready(function(){
            @if (session('sucesso'))
                iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "{{session('sucesso')}}",
                });
            @endif
            @if (session('alerta'))
                iziToast.error({
                    title: 'O.o',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "{{session('alerta')}}",
                });
            @endif
    
        });
    
    </script>
    

</body>

</html>
