<!DOCTYPE html>
<html>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
        <title>{{ $title }} | Bem da Gente</title>
    
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="font-awesome/css/font-awesome.css" rel="stylesheet">
    
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="/js/iziToast/dist/css/iziToast.min.css" rel="stylesheet" >
    
    </head>

<body class="gray-bg">

    <div class="passwordBox animated rotateIn">
        <div class="row">

            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Esqueceu a Senha?</h2>

                    <p>
                        Digite seu endereço de e-mail e um link de confirmação será enviado por e-mail para você.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                            <form class="m-t" role="form" method="post" action="/RedefinirSenha">
                                {!! csrf_field() !!}
                                <div class="form-group">
                                    <input type="text" autofocus autocomplete="off" class="form-control" name="usuario" placeholder="Usuário" required="">
                                </div>

                                <button type="submit" class="btn btn-primary block full-width m-b">Enviar</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-8">
                <small>Todos os direitos reservados Bem da gente</small>
            </div>
            <div class="col-md-3 text-right">
               <small>&copy; {{date('Y')}}</small>
            </div>
        </div>
    </div>
    <!-- Mainly scripts -->
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script src="/js/iziToast/dist/js/iziToast.min.js"></script>
    <script>
        $(document).ready(function(){
            @if (session('sucesso'))
                iziToast.success({
                    title: ':)',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "{{session('sucesso')}}",
                });
            @endif
            @if (session('alerta'))
                iziToast.error({
                    title: 'O.o',
                    transitionIn: 'bounceInLeft',
                    position: 'topRight',
                    message: "{{session('alerta')}}",
                });
            @endif
    
        });
    
    </script>
    

</body>

</html>
